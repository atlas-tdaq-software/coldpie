# coldpie


- tag `coldpie-00-06-00`
- dropped Python2 support
- tag `coldpie-00-05-01`
- add `coral.pyi` and `cool.pyi` type-annotated interface specifications

## tdaq-11-02-01

## tdaq-11-02-00

- tag `coldpie-00-05-00`
- Add keyword-only `order` argument to ChannelSelection constructor

## tdaq-11-01-00

## tdaq-10-00-00

- tag `coldpie-00-04-01`
- add few missing methods to Folder and FolderSet wrappers
- Folder and FolderSet now have Python base class HvsNode which is a wrapper
  for cool::IHvsNode

## tdaq-09-04-00

- tag `coldpie-00-03-06`
- add few missing methods to Database class wrapper

## tdaq-09-02-00

- tag `coldpie-00-03-04`
- small improvement to enum wrapper classes to get correct module name

## tdaq-09-01-00

- tag `coldpie-00-03-03`
- Added few missing features that were not implemented before
- Record indexing operator now returns field value instead of Field instance

## tdaq-09-00-00

- tag `coldpie-00-02-02`
