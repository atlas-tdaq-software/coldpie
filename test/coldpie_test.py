#!/bin/env tdaq_python
#
#

import contextlib
import os
import sys
import tempfile
import time
import unittest

from coldpie import cool
from coldpie.coral import Blob


@contextlib.contextmanager
def makeTmpFile():
    """Context manager for generating temporary file name.

    Temporary file is deleted on exiting context.
    """
    fd, tmpname = tempfile.mkstemp()
    os.close(fd)
    yield tmpname
    try:
        os.remove(tmpname)
    except OSError:
        pass


class TestCoolpy(unittest.TestCase):

    def test_01_enums(self):
        # test for module level constants and enums
        self.assertEqual(cool.ValidityKeyMin, 0)
        self.assertEqual(cool.ValidityKeyMax, 2 ** 63 - 1)

        versioning_modes = [cool.FolderVersioning.NONE,
                            cool.FolderVersioning.SINGLE_VERSION,
                            cool.FolderVersioning.MULTI_VERSION]

        payload_modes = [cool.PayloadMode.INLINEPAYLOAD,
                         cool.PayloadMode.SEPARATEPAYLOAD,
                         cool.PayloadMode.VECTORPAYLOAD]

        storage_types = [
            cool.StorageType.Bool,
            cool.StorageType.UChar,
            cool.StorageType.Int16,
            cool.StorageType.UInt16,
            cool.StorageType.Int32,
            cool.StorageType.UInt32,
            cool.StorageType.UInt63,
            cool.StorageType.Int64,
            cool.StorageType.Float,
            cool.StorageType.Double,
            cool.StorageType.String255,
            cool.StorageType.String4k,
            cool.StorageType.String64k,
            cool.StorageType.String16M,
            cool.StorageType.String128M,
            cool.StorageType.Blob64k,
            cool.StorageType.Blob16M,
            cool.StorageType.Blob128M,
        ]


    def test_02_FieldSpecification(self):
        # test for FieldSpecification class
        x = cool.FieldSpecification("a", cool.StorageType.Bool)
        self.assertEqual(x.name(), "a")
        self.assertEqual(x.storageType(), cool.StorageType.Bool)
        self.assertEqual(str(x), "{a:Bool}")

        # comparison
        x1 = cool.FieldSpecification("a", cool.StorageType.Bool)
        y = cool.FieldSpecification("b", cool.StorageType.Int32)
        self.assertEqual(x, x)
        self.assertEqual(x, x1)
        self.assertEqual(x1, x)
        self.assertNotEqual(x, y)
        self.assertNotEqual(y, x)
        self.assertNotEqual(x, 1.)
        self.assertNotEqual(1, y)
        self.assertNotEqual([], x)

    def test_03_RecordSpecification(self):
        # test for RecordSpecification class
        fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                  cool.FieldSpecification("b", cool.StorageType.Double),
                  cool.FieldSpecification("c", cool.StorageType.String16M),
                  cool.FieldSpecification("d", cool.StorageType.Blob64k)]

        # construct from list
        rec = cool.RecordSpecification(fields)
        self.assertEqual(len(rec), 4)

        # check exists() and __contains__
        for name in ('a', 'b', 'c', 'd'):
            self.assertTrue(rec.exists(name))
            self.assertTrue(name in rec)
        for field in fields:
            self.assertTrue(field in rec)
        for name in ('x', 'ay'):
            self.assertFalse(rec.exists(name))
            self.assertTrue(name not in rec)
        self.assertTrue(cool.FieldSpecification("a", cool.StorageType.Double) not in rec)

        # index by position
        self.assertEqual(rec[0].name(), "a")
        self.assertEqual(rec[0].storageType(), cool.StorageType.Int32)
        self.assertEqual(rec[1].name(), "b")
        self.assertEqual(rec[1].storageType(), cool.StorageType.Double)
        self.assertEqual(rec[2].name(), "c")
        self.assertEqual(rec[2].storageType(), cool.StorageType.String16M)
        self.assertEqual(rec[3].name(), "d")
        self.assertEqual(rec[3].storageType(), cool.StorageType.Blob64k)

        # negative index
        self.assertEqual(rec[-1].name(), "d")
        self.assertEqual(rec[-2].name(), "c")
        self.assertEqual(rec[-3].name(), "b")
        self.assertEqual(rec[-4].name(), "a")

        with self.assertRaises(IndexError):
            _ = rec[5].name()

        # index by name
        self.assertEqual(rec["a"].name(), "a")
        self.assertEqual(rec["c"].name(), "c")

        with self.assertRaises(LookupError):
            _ = rec["x"].name()

        # iterate over it
        xfields = [x for x in rec]
        self.assertEqual(xfields[0].name(), "a")
        self.assertEqual(xfields[0].storageType(), cool.StorageType.Int32)
        self.assertEqual(xfields[1].name(), "b")
        self.assertEqual(xfields[1].storageType(), cool.StorageType.Double)
        self.assertEqual(xfields[2].name(), "c")
        self.assertEqual(xfields[2].storageType(), cool.StorageType.String16M)
        self.assertEqual(xfields[3].name(), "d")
        self.assertEqual(xfields[3].storageType(), cool.StorageType.Blob64k)

        # construct from from iterator
        rec = cool.RecordSpecification(f for f in fields)
        self.assertEqual(len(rec), 4)
        for name in ('a', 'b', 'c', 'd'):
            self.assertTrue(name in rec)
        for name in ('x', 'ay'):
            self.assertTrue(name not in rec)

        # comparison
        rec1 = cool.RecordSpecification(fields)
        rec2 = cool.RecordSpecification(fields[1:3])
        self.assertEqual(rec, rec)
        self.assertEqual(rec, rec1)
        self.assertEqual(rec1, rec)
        self.assertNotEqual(rec, rec2)
        self.assertNotEqual(rec2, rec1)
        self.assertNotEqual(rec2, rec1)
        self.assertNotEqual(rec, [])
        self.assertNotEqual(rec, None)
        self.assertNotEqual(None, rec1)

    def test_04_FolderSpecification(self):
        # test for RecordSpecification class
        fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                  cool.FieldSpecification("b", cool.StorageType.Double),
                  cool.FieldSpecification("c", cool.StorageType.String16M),
                  cool.FieldSpecification("d", cool.StorageType.Blob64k)]
        rec = cool.RecordSpecification(fields)

        fldr = cool.FolderSpecification(cool.FolderVersioning.SINGLE_VERSION,
                                        rec, cool.PayloadMode.INLINEPAYLOAD)

        payloadSpec = fldr.payloadSpecification()
        self.assertEqual(len(payloadSpec), 4)

        for name in ('a', 'b', 'c', 'd'):
            self.assertTrue(payloadSpec.exists(name))
        for name in ('x', 'ay'):
            self.assertFalse(payloadSpec.exists(name))

        # comparison
        fldr2 = cool.FolderSpecification(cool.FolderVersioning.SINGLE_VERSION,
                                         rec, cool.PayloadMode.INLINEPAYLOAD)
        rec3 = cool.RecordSpecification(fields[1:3])
        fldr3 = cool.FolderSpecification(cool.FolderVersioning.SINGLE_VERSION,
                                         rec3, cool.PayloadMode.INLINEPAYLOAD)
        fldr4 = cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION,
                                         rec, cool.PayloadMode.INLINEPAYLOAD)
        self.assertEqual(fldr, fldr)
        self.assertEqual(fldr, fldr2)
        self.assertEqual(fldr2, fldr)
        self.assertNotEqual(fldr, fldr3)
        self.assertNotEqual(fldr3, fldr)
        self.assertNotEqual(fldr4, fldr2)
        self.assertNotEqual(fldr, [])
        self.assertNotEqual(None, fldr)

    def test_05_Record(self):

        # incorrect arg type
        with self.assertRaises(TypeError):
            cool.Record({})

        # test for Record class
        fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                  cool.FieldSpecification("b", cool.StorageType.Double),
                  cool.FieldSpecification("c", cool.StorageType.String16M),
                  cool.FieldSpecification("d", cool.StorageType.Blob64k)]
        rspec = cool.RecordSpecification(fields)

        rec = cool.Record(rspec)

        self.assertEqual(len(rec), 4)

        self.assertTrue("a" in rec)
        self.assertTrue("x" not in rec)

        spec = rec.specification()
        self.assertEqual(len(spec), 4)

        for idx in range(-4, 4):
            field = rec.field(idx)
            self.assertEqual(field.name(), fields[idx].name())
            self.assertEqual(field.storageType(), fields[idx].storageType())

        with self.assertRaises(IndexError):
            field = rec.field(4)

        for idx in ('a', 'b', 'c', 'd'):
            field = rec.field(idx)
            self.assertEqual(field.name(), idx)

        with self.assertRaises(KeyError):
            field = rec.field('F')

        # check values
        self.assertEqual(rec[0], 0)
        self.assertEqual(rec[1], 0.0)
        self.assertEqual(rec[2], "")
        blob = rec[3]
        self.assertTrue(isinstance(blob, Blob))
        self.assertEqual(blob.size(), 0)

        # test NULL
        for idx in range(4):
            value = rec[idx]
            self.assertFalse(rec.field(idx).isNull())
            self.assertIsNotNone(rec[idx])

            rec.field(idx).setNull()
            if idx == 2:
                # for strings NULL means empty string, and isNull() returns false
                self.assertFalse(rec.field(idx).isNull())
                self.assertTrue(rec[idx] == "")
            else:
                self.assertTrue(rec.field(idx).isNull())
                self.assertIsNone(rec[idx])

            rec[idx] = value
            self.assertFalse(rec.field(idx).isNull())
            self.assertIsNotNone(rec[idx])

            rec[idx] = None
            if idx == 2:
                # for strings NULL means empty string, and isNull() returns false
                self.assertFalse(rec.field(idx).isNull())
                self.assertTrue(rec[idx] == "")
            else:
                self.assertTrue(rec.field(idx).isNull())
                self.assertIsNone(rec[idx])


        # test some values
        rec.field(0).setValue(123)
        self.assertFalse(rec.field(0).isNull())
        self.assertEqual(rec[0], 123)

        rec.field(1).setValue(123.456)
        self.assertFalse(rec.field(1).isNull())
        self.assertEqual(rec[1], 123.456)

        rec.field(2).setValue("abcd")
        self.assertFalse(rec.field(2).isNull())
        self.assertEqual(rec[2], "abcd")

        # blob field can accept any buffer objects
        rec.field(3).setValue(Blob(10))
        self.assertFalse(rec.field(3).isNull())
        self.assertEqual(rec[3].size(), 10)
        rec.field(3).setValue(b"123456789")
        self.assertEqual(rec[3].size(), 9)

        # some incorrect types
        with self.assertRaises(TypeError):
            rec[0] = "abcd"
        with self.assertRaises(TypeError):
            rec[0] = Blob()
        with self.assertRaises(TypeError):
            rec[1] = ""
        with self.assertRaises(TypeError):
            rec[1] = Blob()
        with self.assertRaises(TypeError):
            rec[2] = 1
        with self.assertRaises(TypeError):
            rec[2] = Blob()
        with self.assertRaises(TypeError):
            rec[3] = 1

        # dict-style set value
        rec[0] = 1
        self.assertFalse(rec.field(0).isNull())
        self.assertEqual(rec[0], 1)
        rec['a'] = 2
        self.assertFalse(rec.field(0).isNull())
        self.assertEqual(rec[0], 2)

        # comparison
        rec1 = cool.Record(rspec)
        rec2 = cool.Record(rspec)
        self.assertEqual(rec1, rec2)
        rec1[0] = 1
        self.assertNotEqual(rec1, rec2)
        rec2[0] = 1
        self.assertEqual(rec1, rec2)
        rec1[3] = None
        self.assertNotEqual(rec1, rec2)
        rec2[3] = None
        self.assertEqual(rec1, rec2)

    def test_06_RecordLongType(self):
        # test for Record class support of 63/64-bit integers
        fields = [cool.FieldSpecification("u32", cool.StorageType.UInt32),
                  cool.FieldSpecification("i32", cool.StorageType.Int32),
                  cool.FieldSpecification("u63", cool.StorageType.UInt63),
                  cool.FieldSpecification("i64", cool.StorageType.Int64)]
        rspec = cool.RecordSpecification(fields)

        rec = cool.Record(rspec)

        rec["u32"] = 0
        rec["i32"] = 0
        rec["u63"] = 0
        rec["i64"] = 0

        rec["u32"] = -1
        rec["i32"] = -1
        if sys.version_info[0] == 2:
            with self.assertRaises(ValueError):
                rec["u63"] = -1
        else:
            with self.assertRaises(OverflowError):
                rec["u63"] = -1
        rec["i64"] = -1

        with self.assertRaises(OverflowError):
            rec["u32"] = 0xFFFFFFFFFFFFFFFF
        with self.assertRaises(OverflowError):
            rec["i32"] = 0xFFFFFFFFFFFFFFFF
        with self.assertRaises(ValueError):
            rec["u63"] = 0xFFFFFFFFFFFFFFFF
        with self.assertRaises(OverflowError):
            rec["i64"] = 0xFFFFFFFFFFFFFFFF

        rec["u63"] = 0x7FFFFFFFFFFFFFFF
        rec["i64"] = 0x7FFFFFFFFFFFFFFF
        with self.assertRaises(OverflowError):
            rec["i64"] = 0x8000000000000000

        if sys.version_info[0] == 2:
            # test for Python `long` values, eval is needed to hide L constants
            # from Python3
            rec["u63"] = eval("8000000000L")
            with self.assertRaises(OverflowError):
                rec["u63"] = eval("-8000000000L")
            rec["i64"] = eval("8000000000L")
            rec["i64"] = eval("-8000000000L")

    def test_07_RecordAsSeq(self):

        # test for RecordSpecification class
        fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                  cool.FieldSpecification("b", cool.StorageType.Double),
                  cool.FieldSpecification("c", cool.StorageType.String16M),
                  cool.FieldSpecification("d", cool.StorageType.Blob64k)]
        rspec = cool.RecordSpecification(fields)

        rec = cool.Record(rspec)

        for i, field in enumerate(rec):
            self.assertEqual(field.name(), fields[i].name())

    def test_08_RecordCopy(self):

        # test for Record() copy constructor
        fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                  cool.FieldSpecification("b", cool.StorageType.Double),
                  cool.FieldSpecification("c", cool.StorageType.String16M),
                  cool.FieldSpecification("d", cool.StorageType.Blob64k)]
        rspec = cool.RecordSpecification(fields)

        rec1 = cool.Record(rspec)
        rec2 = cool.Record(rec1)

        # after copy each gets its own copy of data
        self.assertEqual(rec1[0], 0)
        rec1[0] = 1
        self.assertEqual(rec1[0], 1)
        self.assertEqual(rec2[0], 0)
        rec2[0] = 2
        self.assertEqual(rec2[0], 2)
        self.assertEqual(rec1[0], 1)

    def test_09_FolderTest(self):

        # static methods
        self.assertEqual(cool.Folder.headTag(), "HEAD")
        self.assertTrue(cool.Folder.isHeadTag("HEAD"))
        self.assertTrue(cool.Folder.isHeadTag(""))
        self.assertFalse(cool.Folder.isHeadTag("FOOT"))

        dbSvc = cool.DatabaseSvcFactory.databaseService()
        with makeTmpFile() as tmpname:

            db = dbSvc.createDatabase("sqlite://;schema={};dbname=TEST".format(tmpname))

            fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                    cool.FieldSpecification("b", cool.StorageType.Double),
                    cool.FieldSpecification("c", cool.StorageType.String16M),
                    cool.FieldSpecification("d", cool.StorageType.Blob64k)]
            rspec = cool.RecordSpecification(fields)

            # Single-version folder
            fspec = cool.FolderSpecification(cool.FolderVersioning.SINGLE_VERSION, rspec)
            folder = db.createFolder("/TEST_SV", fspec, "descr", True)
            self.assertEqual(folder.versioningMode(), cool.FolderVersioning.SINGLE_VERSION)

            # Content is implementation-dependent, can only check type
            self.assertIsInstance(folder.folderAttributes(), cool.Record_const)

            self.assertIsInstance(folder.id(), int)
            self.assertIsInstance(folder.parentId(), int)
            self.assertEqual(folder.description(), "descr")
            folder.setDescription("New description")
            self.assertEqual(folder.description(), "New description")
            self.assertEqual(folder.listTags(), [])

            # Multi-version folder
            fspec = cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, rspec)
            folder = db.createFolder("/TEST_MV", fspec, "descr", True)
            self.assertEqual(folder.versioningMode(), cool.FolderVersioning.MULTI_VERSION)

            self.assertEqual(folder.listTags(), [])
            with self.assertRaisesRegex(RuntimeError, "coldpie error: Tag 'PARENT-TAG' not found"):
                folder.resolveTag("PARENT-TAG")
            folder.createTagRelation("PARENT-TAG", "TAG")
            self.assertEqual(folder.listTags(), ["TAG"])

            self.assertEqual(folder.findTagRelation("PARENT-TAG"), "TAG")
            self.assertEqual(folder.resolveTag("PARENT-TAG"), "TAG")

            self.assertEqual(folder.tagDescription("TAG"), "")
            folder.setTagDescription("TAG", "Beautiful tag")
            self.assertEqual(folder.tagDescription("TAG"), "Beautiful tag")

            self.assertEqual(folder.tagLockStatus("TAG"), cool.HvsTagLock.UNLOCKED)
            folder.setTagLockStatus("TAG", cool.HvsTagLock.LOCKED)
            self.assertEqual(folder.tagLockStatus("TAG"), cool.HvsTagLock.LOCKED)
            folder.setTagLockStatus("TAG", cool.HvsTagLock.PARTIALLYLOCKED)
            self.assertEqual(folder.tagLockStatus("TAG"), cool.HvsTagLock.PARTIALLYLOCKED)
            folder.setTagLockStatus("TAG", cool.HvsTagLock.UNLOCKED)
            self.assertEqual(folder.tagLockStatus("TAG"), cool.HvsTagLock.UNLOCKED)

            folder.deleteTagRelation("PARENT-TAG")
            with self.assertRaisesRegex(RuntimeError, "coldpie error: Tag 'PARENT-TAG' not found"):
                folder.resolveTag("PARENT-TAG")

    def test_10_FullWriteTest(self):

        dbSvc = cool.DatabaseSvcFactory.databaseService()
        with makeTmpFile() as tmpname:

            db = dbSvc.createDatabase("sqlite://;schema={};dbname=TEST".format(tmpname))

            fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                    cool.FieldSpecification("b", cool.StorageType.Double),
                    cool.FieldSpecification("c", cool.StorageType.String16M),
                    cool.FieldSpecification("d", cool.StorageType.Blob64k)]
            rspec = cool.RecordSpecification(fields)
            fspec = cool.FolderSpecification(cool.FolderVersioning.SINGLE_VERSION, rspec)

            folder = db.createFolder("/TEST", fspec, "descr", True)

            end = 2000
            for iov in range(1000, end, 100):
                rec = cool.Record(rspec)
                rec['a'] = iov + 1
                rec['b'] = iov * iov
                rec['c'] = "iov: " + str(iov)
                rec['d'] = b"some data here"
                folder.storeObject(iov, iov + 100, rec, 0)
                folder.storeObject(iov, iov + 100, rec, 100)

            for iov in range(1000, end, 100):
                obj = folder.findObject(iov + 1, 0)
                self.assertEqual(obj.channelId(), 0)
                self.assertEqual(obj.since(), iov)
                self.assertEqual(obj.until(), iov + 100)
                rec = obj.payload()
                self.assertEqual(rec['a'], iov + 1)
                self.assertTrue(rec['d'])

            for iov in range(1000, end, 100):
                itr = folder.findObjects(iov + 1, cool.ChannelSelection(0))
                objects = list(itr)
                self.assertEqual(len(objects), 1)
                itr = folder.findObjects(iov + 1, cool.ChannelSelection(100))
                objects = list(itr)
                self.assertEqual(len(objects), 1)
                itr = folder.findObjects(iov + 1, cool.ChannelSelection())
                objects = list(itr)
                self.assertEqual(len(objects), 2)

            itr = folder.browseObjects(1000, end, cool.ChannelSelection(0))
            objects = list(itr)
            self.assertEqual(len(objects), 10)
            itr = folder.browseObjects(1000, end, cool.ChannelSelection(100))
            objects = list(itr)
            self.assertEqual(len(objects), 10)
            itr = folder.browseObjects(1000, end, cool.ChannelSelection.all())
            objects = list(itr)
            self.assertEqual(len(objects), 20)

            # store Record_const
            obj = folder.findObject(1001, 0)
            rec = obj.payload()
            self.assertIsInstance(rec, cool.Record_const)
            folder.storeObject(end, end + 100, rec, 0)

            # copy Record_const into Record and update it
            obj = folder.findObject(1001, 0)
            rec = obj.payload()
            self.assertIsInstance(rec, cool.Record_const)
            rec = cool.Record(rec)
            rec["a"] = 500
            folder.storeObject(end+100, end + 200, rec, 0)

            # wrong type generates exception
            with self.assertRaises(TypeError):
                folder.storeObject(end + 100, end + 200, dict(), 0)

    def test_11_FolderChannels(self):
        """Testing channel-related methods"""

        dbSvc = cool.DatabaseSvcFactory.databaseService()
        with makeTmpFile() as tmpname:

            db = dbSvc.createDatabase("sqlite://;schema={};dbname=TEST".format(tmpname))

            fields = [cool.FieldSpecification("a", cool.StorageType.Int32),
                    cool.FieldSpecification("b", cool.StorageType.Double),
                    cool.FieldSpecification("c", cool.StorageType.String16M),
                    cool.FieldSpecification("d", cool.StorageType.Blob64k)]
            rspec = cool.RecordSpecification(fields)

            # Single-version folder
            fspec = cool.FolderSpecification(cool.FolderVersioning.SINGLE_VERSION, rspec)
            folder = db.createFolder("/TEST_SV", fspec, "descr", True)

            self.assertEqual(folder.listChannels(), [])
            self.assertEqual(folder.listChannelsWithNames(), {})

            folder.createChannel(123, "Channel123")
            self.assertEqual(folder.listChannels(), [123])
            self.assertEqual(folder.listChannelsWithNames(), {123: "Channel123"})
            self.assertEqual(folder.channelName(123), "Channel123")
            self.assertEqual(folder.channelId("Channel123"), 123)
            self.assertEqual(folder.channelDescription(123), "")

            self.assertTrue(folder.existsChannel("Channel123"))
            self.assertTrue(folder.existsChannel(123))

            self.assertFalse(folder.existsChannel(111))
            self.assertFalse(folder.existsChannel("channel"))

            folder.createChannel(1234, "Channel1234", "Description1234")
            self.assertEqual(folder.channelDescription(1234), "Description1234")
            folder.setChannelDescription(1234, "4321 what is that")
            self.assertEqual(folder.channelDescription(1234), "4321 what is that")

            self.assertEqual(folder.listChannelsWithNames(), {123: "Channel123", 1234: "Channel1234"})
            self.assertFalse(folder.dropChannel(111))
            self.assertTrue(folder.dropChannel(123))
            self.assertEqual(folder.listChannelsWithNames(), {1234: "Channel1234"})

    def test_reopen_database(self):

        dbSvc = cool.DatabaseSvcFactory.databaseService()
        with makeTmpFile() as tmpname:

            db = dbSvc.createDatabase("sqlite://;schema={};dbname=TEST".format(tmpname))
            self.assertTrue(db.isOpen())

            db.closeDatabase()
            self.assertFalse(db.isOpen())

            db.openDatabase()
            self.assertTrue(db.isOpen())

    def test_20_Time(self):

        # check that empty arg list returns current time
        t = cool.Time()
        self.assertTrue(time.time() - t.total_nanoseconds() / 1e9 < 0.1)

        # all explicit values
        t = cool.Time(2000, 1, 2, 3, 4, 5)
        self.assertEqual(t.year(), 2000)
        self.assertEqual(t.month(), 1)
        self.assertEqual(t.day(), 2)
        self.assertEqual(t.hour(), 3)
        self.assertEqual(t.minute(), 4)
        self.assertEqual(t.second(), 5)
        self.assertEqual(t.nanosecond(), 0)

        # with nanosecond
        t = cool.Time(2000, 12, 31, 23, 59, 59, 123456789)
        self.assertEqual(t.year(), 2000)
        self.assertEqual(t.month(), 12)
        self.assertEqual(t.day(), 31)
        self.assertEqual(t.hour(), 23)
        self.assertEqual(t.minute(), 59)
        self.assertEqual(t.second(), 59)
        self.assertEqual(t.nanosecond(), 123456789)

        # single parameter
        t = cool.Time(0)
        self.assertEqual(t.year(), 1970)
        self.assertEqual(t.month(), 1)
        self.assertEqual(t.day(), 1)
        self.assertEqual(t.hour(), 0)
        self.assertEqual(t.minute(), 0)
        self.assertEqual(t.second(), 0)
        self.assertEqual(t.nanosecond(), 0)

        t = cool.Time(1556332985123456789)
        self.assertEqual(t.year(), 2019)
        self.assertEqual(t.month(), 4)
        self.assertEqual(t.day(), 27)
        self.assertEqual(t.hour(), 2)
        self.assertEqual(t.minute(), 43)
        self.assertEqual(t.second(), 5)
        self.assertEqual(t.nanosecond(), 123456789)

    def test_21_Time_errors(self):

        # number of arguments
        args = [(1, 1),
                (1, 1, 1),
                (1, 1, 1, 1),
                (1, 1, 1, 1, 1),
                (1, 1, 1, 1, 1, 1, 1, 1),
                ]
        for argtuple in args:
            with self.assertRaises(TypeError):
                cool.Time(*argtuple)

        # acceptable ranges
        args = [(0, 1, 1, 1, 1, 1, 0),
                (2000, 0, 1, 1, 1, 1, 0),
                (2000, 13, 1, 1, 1, 1, 0),
                (2000, 1, 0, 1, 1, 1, 0),
                (2000, 1, 32, 1, 1, 1, 0),
                (2000, 1, 1, -1, 1, 1, 0),
                (2000, 1, 1, 24, 1, 1, 0),
                (2000, 1, 1, 0, -1, 1, 0),
                (2000, 1, 1, 0, 60, 1, 0),
                (2000, 1, 1, 0, 0, -1, 0),
                (2000, 1, 1, 0, 0, 60, 0),
                (2000, 1, 1, 0, 0, 0, -1),
                (2000, 1, 1, 0, 0, 0, 1000000000),
                ]
        for argtuple in args:
            with self.assertRaises(ValueError):
                cool.Time(*argtuple)

    def test_21_Time_compare(self):

        t1 = cool.Time(2000, 1, 1, 0, 0, 0, 123456789)
        t1a = cool.Time(2000, 1, 1, 0, 0, 0, 123456789)
        t2 = cool.Time(2001, 1, 1, 0, 0, 0, 123456789)

        with self.assertRaises(TypeError):
            x = t1 > self
        with self.assertRaises(TypeError):
            x = self < t2

        self.assertTrue(t1 < t2)
        self.assertTrue(t1 <= t2)
        self.assertTrue(not (t1 == t2))
        self.assertTrue(t1 != t2)
        self.assertTrue(not (t1 > t2))
        self.assertTrue(not (t1 >= t2))

        self.assertTrue(not (t1 < t1a))
        self.assertTrue(t1 <= t1a)
        self.assertTrue(t1 == t1)
        self.assertTrue(t1 == t1a)
        self.assertTrue(not (t1 != t1a))
        self.assertTrue(not (t1 > t1a))
        self.assertTrue(t1 >= t1a)

    def test_22_Time_hash(self):

        t1 = cool.Time(2000, 1, 1, 0, 0, 0, 123456789)
        t2 = cool.Time(2000, 1, 1, 0, 0, 0, 123456789)
        self.assertEqual(hash(t1), hash(t2))

        d = {t1: 1}
        self.assertEqual(d[t1], 1)
        self.assertEqual(d[t2], 1)

    def test_22_ChannelSelection(self):

        # we only use ChannelSelection to pass it to COOL methods
        # so it has no exposed methods, but we can test construction

        # all channels
        chsel = cool.ChannelSelection()
        chsel = cool.ChannelSelection.all()

        # single channel number
        chsel = cool.ChannelSelection(100)

        # range of channels
        chsel = cool.ChannelSelection(100, 110)

        # channel name
        chsel = cool.ChannelSelection("ch1")

        # all kinds of invalid arguments
        with self.assertRaises(TypeError):
            chsel = cool.ChannelSelection(1.)
        with self.assertRaises(TypeError):
            chsel = cool.ChannelSelection([])
        with self.assertRaises(TypeError):
            chsel = cool.ChannelSelection(None)
        with self.assertRaises(TypeError):
            chsel = cool.ChannelSelection(1, "ch1")
        with self.assertRaises(TypeError):
            chsel = cool.ChannelSelection("ch1", "ch2")
        with self.assertRaises(TypeError):
            chsel = cool.ChannelSelection(1, 2, "")

        # tests with order argument
        chsel = cool.ChannelSelection(order=cool.ChannelSelection.Order.sinceBeforeChannel)
        chsel = cool.ChannelSelection.all(order=cool.ChannelSelection.Order.sinceBeforeChannel)

        chsel = cool.ChannelSelection(1, 100, order=cool.ChannelSelection.Order.channelBeforeSince)
        chsel = cool.ChannelSelection("ch1", order=cool.ChannelSelection.Order.channelBeforeSince)

        with self.assertRaises(ValueError):
            chsel = cool.ChannelSelection.all(order=100)
        with self.assertRaises(ValueError):
            chsel = cool.ChannelSelection(order=100)
        with self.assertRaises(ValueError):
            chsel = cool.ChannelSelection("ch1", order=-1)
        with self.assertRaises(ValueError):
            chsel = cool.ChannelSelection(1, 100, order=48)


if __name__ == "__main__":
    unittest.main()
