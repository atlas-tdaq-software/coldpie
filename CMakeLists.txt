tdaq_package()

tdaq_add_library( _coldpie
		  src/*.cxx
		  INCLUDE_DIRECTORIES PRIVATE ${PYTHON_INCLUDE_DIRS} CORAL::CoralBase
		  LINK_LIBRARIES Python::Development COOL::CoolKernel CORAL::CoralBase COOL::CoolApplication COOL::RelationalCool )

tdaq_add_python_package( coldpie )
tdaq_add_python_files( python/coldpie/*.pyi python/coldpie/py.typed DESTINATION coldpie/ )

# unit tests
tdaq_add_test(NAME coldpie_test COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/test/coldpie_test.py POST_INSTALL)
