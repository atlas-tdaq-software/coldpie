#ifndef COLDPIE_PYSUBCLASS_H
#define COLDPIE_PYSUBCLASS_H

//--------------------------------------------------------------------------
//
// Description:
//  Class PySubclass.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include "Python.h"
#include <new>
#include <string>
#include <sstream>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//    ---------------------
//    -- Class Interface --
//    ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  @brief C++ class which defines Python object type which is a sub-class of
 *     PyDataClass or other PySubclass.
 *
 *  Template parameter ConcreteType defines final C++ type (class that inherits
 *  this one). BaseType defines base class for this one which could be
 *  PyDataType or PySubclass. This class should be used for cases when base
 *  class holds a polymorphic object, e.g. shared pointer to some base class.
 */

template <typename ConcreteType, typename SubCppType, typename BaseType>
struct PySubclass : BaseType {

    /// Returns the Python type (borrowed)
    static PyTypeObject* typeObject();

    /// Builds Python object from corresponding C++ type (uses copy constructor for SubCppType).
    /// If owner pointer is non-null it
    static ConcreteType* PyObject_FromCpp(const SubCppType& obj, PyObject* owner=nullptr);

    /// Builds Python object from corresponding C++ type (uses move constructor for SubCppType)
    static ConcreteType* PyObject_FromCpp(SubCppType&& obj, PyObject* owner=nullptr);

    // This method should be used from tp_new method (if you provide one)
    template<typename... Args>
    static
    ConcreteType* construct(PyTypeObject* subtype, Args&&... args) {
        return PyObject_FromCpp(SubCppType(std::forward<Args>(args)...));
    }

    // returns true if object is an instance of this type or subtype
    static bool Object_TypeCheck(PyObject* obj) {
        PyTypeObject* type = typeObject();
        return PyObject_TypeCheck( obj, type );
    }

protected:


    /// Initialize Python type and register it in a module, if modname is not provided
    /// then module name is used
    static void initType( const char* name, PyObject* module, const char* modname = 0 );

    // Standard Python deallocation function
    static void _dealloc( PyObject* self );

    // repr() function
    static PyObject* repr( PyObject *self )  {
        std::ostringstream str;
        static_cast<ConcreteType*>(self)->print(str);
#if PY_MAJOR_VERSION >= 3
        return PyUnicode_FromString( str.str().c_str() );
#else
        return PyString_FromString( str.str().c_str() );
#endif
    }
};

/// stream insertion operator
template <typename ConcreteType, typename SubCppType, typename BaseType>
std::ostream&
operator<<(std::ostream& out, const PySubclass<ConcreteType, SubCppType, BaseType>& data) {

    static_cast<const ConcreteType&>(data).print(out);
    return out;
}

/// Returns the Python type object
template <typename ConcreteType, typename SubCppType, typename BaseType>
PyTypeObject*
PySubclass<ConcreteType, SubCppType, BaseType>::typeObject()
{
    static PyTypeObject type = {
        PyVarObject_HEAD_INIT(0, 0)
        0,                       /*tp_name*/
        sizeof(ConcreteType),    /*tp_basicsize*/
        0,                       /*tp_itemsize*/
        /* methods */
        _dealloc,                /*tp_dealloc*/
        0,                       /*tp_print*/
        0,                       /*tp_getattr*/
        0,                       /*tp_setattr*/
        0,                       /*tp_compare, Py3 tp_as_async*/
        repr,                    /*tp_repr*/
        0,                       /*tp_as_number*/
        0,                       /*tp_as_sequence*/
        0,                       /*tp_as_mapping*/
        0,                       /*tp_hash*/
        0,                       /*tp_call*/
        repr,                    /*tp_str*/
        0,                       /*tp_getattro*/
        0,                       /*tp_setattro*/
        0,                       /*tp_as_buffer*/
        Py_TPFLAGS_DEFAULT,      /*tp_flags*/
        0,                       /*tp_doc*/
        0,                       /*tp_traverse*/
        0,                       /*tp_clear*/
        0,                       /*tp_richcompare*/
        0,                       /*tp_weaklistoffset*/
        0,                       /*tp_iter*/
        0,                       /*tp_iternext*/
        0,                       /*tp_methods*/
        0,                       /*tp_members*/
        0,                       /*tp_getset*/
        0,                       /*tp_base*/
        0,                       /*tp_dict*/
        0,                       /*tp_descr_get*/
        0,                       /*tp_descr_set*/
        0,                       /*tp_dictoffset*/
        0,                       /*tp_init*/
        PyType_GenericAlloc,     /*tp_alloc*/
        0,                       /*tp_new*/
#if PY_MAJOR_VERSION >= 3
        PyObject_Del,            /*tp_free*/
#else
        _PyObject_Del,           /*tp_free*/
#endif
        0,                       /*tp_is_gc*/
        0,                       /*tp_bases*/
        0,                       /*tp_mro*/
        0,                       /*tp_cache*/
        0,                       /*tp_subclasses*/
        0,                       /*tp_weaklist*/
#if PY_MAJOR_VERSION >= 3
        0,                       /*tp_del*/
        0,                       /*tp_version_tag*/
        0,                       /*tp_finalize*/
#endif
    };

    return &type;
}

template <typename ConcreteType, typename SubCppType, typename BaseType>
void
PySubclass<ConcreteType, SubCppType, BaseType>::_dealloc(PyObject* self)
{
    BaseType::_dealloc(self);
}

/// Builds Python object from corresponding C++ type.
template <typename ConcreteType, typename SubCppType, typename BaseType>
ConcreteType*
PySubclass<ConcreteType, SubCppType, BaseType>::PyObject_FromCpp(const SubCppType& obj, PyObject* owner_)
{
    ConcreteType* ob = PyObject_New(ConcreteType, typeObject());
    if (not ob) {
        PyErr_SetString(PyExc_RuntimeError, "Failed to create PySubclass object.");
        return 0;
    }

    // copy-construct the object
    new(&ob->cpp_obj) decltype(ob->cpp_obj)(SubCppType(obj));
    ob->owner = owner_;
    Py_XINCREF(owner_);

    return ob;
}

/// Builds Python object from corresponding C++ type.
template <typename ConcreteType, typename SubCppType, typename BaseType>
ConcreteType*
PySubclass<ConcreteType, SubCppType, BaseType>::PyObject_FromCpp(SubCppType&& obj, PyObject* owner_)
{
    ConcreteType* ob = PyObject_New(ConcreteType, typeObject());
    if (not ob) {
        PyErr_SetString(PyExc_RuntimeError, "Failed to create PySubclass object.");
        return 0;
    }

    // copy-construct the object
    new(&ob->cpp_obj) decltype(ob->cpp_obj)(SubCppType(std::move(obj)));
    ob->owner = owner_;
    Py_XINCREF(owner_);

    return ob;
}

/// Initialize Python type and register it in a module
template <typename ConcreteType, typename SubCppType, typename BaseType>
void
PySubclass<ConcreteType, SubCppType, BaseType>::initType(const char* name, PyObject* module, const char* modname)
{
    static std::string typeName;

    // prefix type name with module name
    if (not modname) {
        modname = PyModule_GetName(module);
    }
    if ( modname ) {
        typeName = modname;
        if (not typeName.empty()) typeName += '.';
    }
    typeName += name;

    // set the name
    PyTypeObject* type = typeObject();
    type->tp_name = (char*)typeName.c_str();

    // set base class, initType for base class should be called in advance.
    type->tp_base = BaseType::typeObject();

    // initialize type
    if (PyType_Ready( type ) < 0) return;

    // register it in a module
    PyModule_AddObject(module, name, (PyObject*) type);
}

} // namespace coldpie

#endif // COLDPIE_PYSUBCLASS_H
