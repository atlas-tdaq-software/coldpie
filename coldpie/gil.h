#ifndef COLDPIE_GIL_H
#define COLDPIE_GIL_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include "Python.h"

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Class for exception-safe GIL management.
 *
 *  Typical use:
 *
 *    try {
 *        SomeType value;
 *        {
 *            gil gil;   // This releases GIL
 *            // do something pure C++ which can throw
 *            value = callMethodWithException();
 *            // GIL will be re-acquired by gil destructor on leaving this block
 *        }
 *        // DO something that can touch Python stuff
 *        return PyObject_FromSomething(value);
 *    } catch (const std::exception& exc) {
 *        // GIL will be acquired here so we can do Python stuff
 *        PyErr_SetString(PyExc_RuntimeError, exc.what());
 *        return 0;
 *    }
 *
 *  @author Andy Salnikov
 */

class gil {
public:

    // Constructor releases GIL
    gil() : m_save(PyEval_SaveThread()) {}

    // Destructor re-acquires GIL
    ~gil() { acquire(); }

    // explicit re-acquiring of GIL
    void acquire() {
        if (m_save) {
            PyEval_RestoreThread(m_save);
            m_save = nullptr;
        }
    }

private:

    PyThreadState* m_save;
};

} // namespace coldpie

#endif // COLDPIE_GIL_H
