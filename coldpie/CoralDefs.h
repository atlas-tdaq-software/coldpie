#ifndef COLDPIE_CORALDEFS_H
#define COLDPIE_CORALDEFS_H

/**
 *  Bunch of macros (yuck) to handle CORAL/COOL peculiarities
 */

#include "Python.h"
#include "CoralBase/VersionInfo.h"

#define COLDPIE_CORAL_VERSION   CORAL_VERSIONINFO_RELEASE_MAJOR * 10000 + \
                                CORAL_VERSIONINFO_RELEASE_MINOR * 100 + \
                                CORAL_VERSIONINFO_RELEASE_PATCH

// Flag to tell when to use PyCapsule instead of PyCObject
#define COLDPIE_USE_PYCAPSULE (PY_MAJOR_VERSION >= 3 || COLDPIE_CORAL_VERSION >= 30201)

#endif // COLDPIE_CORALDEFS_H
