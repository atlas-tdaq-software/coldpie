#ifndef COLDPIE_CPPOBJECTHOLDER_H
#define COLDPIE_CPPOBJECTHOLDER_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python class which stores and controls lifetimes of C++ objects.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct CppObjectHolder : public PyDataType<CppObjectHolder, std::shared_ptr<void>> {

    typedef PyDataType<CppObjectHolder, std::shared_ptr<void>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

};

} // namespace coldpie

#endif // COLDPIE_CPPOBJECTHOLDER_H
