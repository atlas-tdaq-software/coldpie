//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "FieldSpecification.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/FieldSpecification.h"
#include "CppObjectHolder.h"
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// standard Python stuff
PyObject* _new(PyTypeObject* subtype, PyObject* args, PyObject* /*kwds*/)
{
    // parse arguments
    const char* name = nullptr;
    int stype = 0;
    if (not PyArg_ParseTuple(args, "si:FieldSpecification", &name, &stype)) {
        return 0;
    }

    auto stypeEnum = static_cast<cool::StorageType::TypeId>(stype);
    try {
        auto cppSpec = std::make_shared<cool::FieldSpecification>(name, stypeEnum);
        PyObject* store = coldpie::CppObjectHolder::PyObject_FromCpp(cppSpec);
        auto self = coldpie::FieldSpecification::construct(subtype, *cppSpec);
        self->owner = store;    // this steals reference
        return self;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return 0;
    }
}

// type-specific methods
PyObject* _name(PyObject* self, PyObject*)
try {
    auto& fs = coldpie::FieldSpecification::cppObject(self).get();
    return coldpie::cpp2py::convert(fs.name());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _storageType(PyObject* self, PyObject*)
try {
    auto& fs = coldpie::FieldSpecification::cppObject(self).get();
    return PyLong_FromLong(fs.storageType().id());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject *_richcompare(PyObject *self, PyObject *other, int op)
{
    if (not coldpie::FieldSpecification::Object_TypeCheck(other)) {
        Py_INCREF(Py_NotImplemented);
        return Py_NotImplemented;
    }
    auto& lhs = coldpie::FieldSpecification::cppObject(self).get();
    auto& rhs = coldpie::FieldSpecification::cppObject(other).get();
    switch (op) {
    case Py_EQ:
        return coldpie::cpp2py::convert(lhs == rhs);
        break;
    case Py_NE:
        return coldpie::cpp2py::convert(lhs != rhs);
        break;
    default:
        break;
    }
    Py_INCREF(Py_NotImplemented);
    return Py_NotImplemented;
}

PyMethodDef methods[] = {
    {"name", _name, METH_NOARGS,
        "self.name() -> str\n\nReturn field name."},
    {"storageType", _storageType, METH_NOARGS,
        "self.storageType() -> int\n\n"
        "Return storage type of the field, possible values are defined in\n"
        "StorageType class."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
FieldSpecification class describes field/attribute of a database record,\n\
it has a name and associated storage type.\n\
\n\
Constructor:\n\
\n\
    FieldSpecification(name: str, storageType: int)\n\
\n\
    Parameters:\n\
        name : str\n\
            Field name.\n\
        storageType : int\n\
            Type of the field, values are defined in StorageType class.\n\
\n\
Instances of FieldSpecification class can be compared for (non-)equality\n\
using `==` or `!=` operators. Ordering relation is not defined for this\n\
class, less/greater operators do not work on objects.\n\
";

} // namespace

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------
void
coldpie::FieldSpecification::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_new = ::_new;
  type->tp_richcompare = ::_richcompare;

  BaseType::initType("FieldSpecification", module, modname);
}

// Dump object info to a stream
void
coldpie::FieldSpecification::print(std::ostream& out) const
{
  out << "{" << cpp_obj.get().name() << ":" << cpp_obj.get().storageType().name() << "}";
}
