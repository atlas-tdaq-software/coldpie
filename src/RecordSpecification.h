#ifndef COLDPIE_RECORDSPECIFICATION_H
#define COLDPIE_RECORDSPECIFICATION_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <functional>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
#include "CoolKernel/IRecordSpecification.h"

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL (I)RecordSpecification class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

struct RecordSpecification : public PyDataType<RecordSpecification, std::reference_wrapper<const cool::IRecordSpecification>> {

    typedef PyDataType<RecordSpecification, std::reference_wrapper<const cool::IRecordSpecification>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_RECORDSPECIFICATION_H
