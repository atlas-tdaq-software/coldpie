//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "Database.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coldpie/gil.h"
#include "cpp2py.h"
#include "exceptions.h"
#include "FolderSpecification.h"
#include "Folder.h"
#include "FolderSet.h"
#include "Record.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------


namespace {

PyObject* _databaseId(PyObject* self, PyObject*)
try {
    auto db = coldpie::Database::cppObject(self);
    return coldpie::cpp2py::convert(db->databaseId());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _databaseName(PyObject* self, PyObject*)
try {
    auto db = coldpie::Database::cppObject(self);
    return coldpie::cpp2py::convert(db->databaseName());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _isOpen(PyObject* self, PyObject*)
try {
    auto db = coldpie::Database::cppObject(self);
    return coldpie::cpp2py::convert(db->isOpen());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _openDatabase(PyObject* self, PyObject*)
try {
    auto db = coldpie::Database::cppObject(self);
    db->openDatabase();
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _closeDatabase(PyObject* self, PyObject*)
try {
    auto db = coldpie::Database::cppObject(self);
    db->closeDatabase();
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _databaseAttributes(PyObject* self, PyObject*)
try {
    auto db = coldpie::Database::cppObject(self);
    auto& attr = db->databaseAttributes();
    return coldpie::Record_const::PyObject_FromCpp(attr, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _existsFolder(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    if (not PyArg_ParseTuple(args, "s:Database.existsFolder", &path)) return 0;

    auto db = coldpie::Database::cppObject(self);
    bool exists;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        exists = db->existsFolder(path);
    }
    return coldpie::cpp2py::convert(exists);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _existsFolderSet(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    if (not PyArg_ParseTuple(args, "s:Database.existsFolderSet", &path)) return 0;

    auto db = coldpie::Database::cppObject(self);
    bool exists;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        exists = db->existsFolderSet(path);
    }
    return coldpie::cpp2py::convert(exists);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _createFolder(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    PyObject* spec = 0;
    const char* descr = "";
    int makeParents = false;
    if (not PyArg_ParseTuple(args, "sO|si:Database.createFolder", &path, &spec, &descr, &makeParents)) return 0;

    if (not coldpie::FolderSpecification::Object_TypeCheck(spec)) {
        PyErr_SetString(PyExc_TypeError, "Expecting FolderSpecification as second argument");
        return 0;
    }
    auto& fspec = coldpie::FolderSpecification::cppObject(spec).get();

    auto db = coldpie::Database::cppObject(self);
    cool::IFolderPtr folder;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder = db->createFolder(path, fspec, descr, bool(makeParents));
    }
    // it should never return zero pointer
    return coldpie::Folder::PyObject_FromCpp(folder);

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _createFolderSet(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    const char* descr = "";
    int makeParents = false;
    if (not PyArg_ParseTuple(args, "s|si:Database.createFolderSet", &path, &descr, &makeParents)) return 0;

    auto db = coldpie::Database::cppObject(self);
    cool::IFolderSetPtr folderSet;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folderSet = db->createFolderSet(path, descr, bool(makeParents));
    }
    // it should never return zero pointer
    return coldpie::FolderSet::PyObject_FromCpp(folderSet);

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _getFolder(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    if (not PyArg_ParseTuple(args, "s:Database.getFolder", &path)) return 0;

    auto db = coldpie::Database::cppObject(self);
    cool::IFolderPtr folder;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder = db->getFolder(path);
    }
    // it should never return zero pointer
    return coldpie::Folder::PyObject_FromCpp(folder);

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _getFolderSet(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    if (not PyArg_ParseTuple(args, "s:Database.getFolderSet", &path)) return 0;

    auto db = coldpie::Database::cppObject(self);
    cool::IFolderSetPtr folderSet;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folderSet = db->getFolderSet(path);
    }
    // it should never return zero pointer
    return coldpie::FolderSet::PyObject_FromCpp(folderSet);

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _dropNode(PyObject* self, PyObject* args)
try {
    const char* path = 0;
    if (not PyArg_ParseTuple(args, "s:Database.dropNode", &path)) return 0;

    auto db = coldpie::Database::cppObject(self);
    bool ok = false;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        ok = db->dropNode(path);
    }
    return coldpie::cpp2py::convert(ok);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _existsTag(PyObject* self, PyObject* args)
try {
    const char* name = 0;
    if (not PyArg_ParseTuple(args, "s:Database.existsTag", &name)) return 0;

    auto db = coldpie::Database::cppObject(self);
    bool exists;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        exists = db->existsTag(name);
    }
    return coldpie::cpp2py::convert(exists);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _listAllNodes(PyObject* self, PyObject* args)
try {
    int ascending = 1;
    if (not PyArg_ParseTuple(args, "|i:Database.listAllNodes", &ascending)) return 0;

    auto db = coldpie::Database::cppObject(self);
    std::vector<std::string> nodes;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        nodes = db->listAllNodes(bool(ascending));
    }
    return coldpie::cpp2py::convert(nodes);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _taggedNodes(PyObject* self, PyObject* args)
try {
    const char* name = 0;
    if (not PyArg_ParseTuple(args, "s:Database.taggedNodes", &name)) return 0;

    auto db = coldpie::Database::cppObject(self);
    std::vector<std::string> nodes;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        nodes = db->taggedNodes(name);
    }
    return coldpie::cpp2py::convert(nodes);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}


PyMethodDef methods[] = {
    {"databaseId", _databaseId, METH_NOARGS,
        "self.databaseId() -> str\n\nReturn database ID."},
    {"databaseName", _databaseName, METH_NOARGS,
        "self.databaseName() -> str\n\nReturn COOL database name."},
    {"isOpen", _isOpen, METH_NOARGS,
        "self.isOpen() -> bool\n\nReturn True if database is open."},
    {"openDatabase", _openDatabase, METH_NOARGS,
        "self.openDatabase()\n\nOpen or re-open the database."},
    {"closeDatabase", _closeDatabase, METH_NOARGS,
        "self.closeDatabase()\n\nClose the database."},
    {"databaseAttributes", _databaseAttributes, METH_NOARGS,
        "self.databaseAttributes() -> Record\n\nReturn database attributes."},
    {"existsFolder", _existsFolder, METH_VARARGS,
        "self.existsFolder(path: str) -> bool\n\nReturn True if given folder path exists."},
    {"existsFolderSet", _existsFolderSet, METH_VARARGS,
        "self.existsFolderSet(name: str) -> bool\n\nReturn True if given folder set exists."},
    {"createFolder", _createFolder, METH_VARARGS,
        "self.createFolder(path: str, spec: FolderSpecification,\n"
        "                  descr: str='', makeParents: bool=False) -> Folder\n\n"
        "Create new folder, using FolderSpecification as input."},
    {"createFolderSet", _createFolderSet, METH_VARARGS,
        "self.createFolderSet(path: str, descr: str='', makeParents: bool=False) -> FolderSet\n\n"
        "Create new folder set."},
    {"getFolder", _getFolder, METH_VARARGS,
        "self.getFolder(path:str) -> Folder\n\nFind a folder, throws exception on errors."},
    {"getFolderSet", _getFolderSet, METH_VARARGS,
        "self.getFolderSet(path: str) -> FolderSet\n\nFind a folderset, throws exception on errors."},
    {"dropNode", _dropNode, METH_VARARGS,
        "self.dropNode(path: str) -> bool\n\nDrop existing folder or folder set. "
        "Returns True on succes, False if node does not exists."},
    {"existsTag", _existsTag, METH_VARARGS,
        "self.existsTag(name: str) -> bool\n\nReturn True if given tag name exists."},
    {"listAllNodes", _listAllNodes, METH_VARARGS,
        "self.listAllNodes(ascending: bool=True) -> list [str]\n\nReturn list of all node names."},
    {"taggedNodes", _taggedNodes, METH_VARARGS,
        "self.taggedNodes(tagName: str) -> list [str]\n\nReturn the names of the nodes where this tag is defined."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL IDatabase C++ class.\n\
\n\
Instances of this class are created in C++ API, there is no user-accessible\n\
constructor at Python level. Use one of the methods of DatabaseSvc class to\n\
make instances of Database class.\n\
";
}

void
coldpie::Database::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("Database", module, modname);
}

// Dump object info to a stream
void
coldpie::Database::print(std::ostream& out) const
{
  out << "cool.Database(" << cpp_obj->databaseId() << ")";
}
