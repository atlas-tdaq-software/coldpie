//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "RecordSpecification.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/FieldSpecification.h"
#include "CoolKernel/RecordException.h"
#include "CoolKernel/RecordSpecification.h"
#include "CppObjectHolder.h"
#include "FieldSpecification.h"
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

struct RecordSpecificationIterData {
    RecordSpecificationIterData(std::reference_wrapper<const cool::IRecordSpecification> const& rec) : record(rec) {}
    std::reference_wrapper<const cool::IRecordSpecification> record;
    unsigned pos = 0;
};

// Special iterator class for Record to iterate over contained fields
struct RecordSpecificationIter : coldpie::PyDataType<RecordSpecificationIter, RecordSpecificationIterData> {

    typedef coldpie::PyDataType<RecordSpecificationIter, RecordSpecificationIterData> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname) {
        PyTypeObject* type = BaseType::typeObject();
        type->tp_doc = "Iterator type for fields in a record instance";
        type->tp_iter = _iter;
        type->tp_iternext = _iternext;
        BaseType::initType("_RecordSpecification_iter", module, modname);
    }

    static PyObject* _iter(PyObject* self) {
        Py_INCREF(self);
        return self;
    }

    static PyObject* _iternext(PyObject* self) {
        auto& iterData = BaseType::cppObject(self);
        if (iterData.pos < iterData.record.get().size()) {
            auto& field = iterData.record.get()[iterData.pos];
            ++ iterData.pos;
            return coldpie::FieldSpecification::PyObject_FromCpp(std::ref(field), self);
        }
        // signal end of iteration
        PyErr_SetString(PyExc_StopIteration, "end of iteration");
        return nullptr;
    }

};


// standard Python stuff
PyObject* _new(PyTypeObject *subtype, PyObject *args, PyObject */*kwds*/)
{
    // parse arguments
    PyObject* arg = nullptr;
    if (not PyArg_ParseTuple(args, "O:RecordSpecification", &arg)) {
        return 0;
    }
    PyObject* iter = PyObject_GetIter(arg);
    if (not iter) return 0;

    // copy fields into C++ object
    try {
        auto cppRecSpec = std::make_shared<cool::RecordSpecification>();

        // iterate over items, make sure they are FieldSpec, copy to a vector
        std::vector<cool::FieldSpecification> fields;
        while (PyObject* item = PyIter_Next(iter)) {
            bool typeMatch = coldpie::FieldSpecification::Object_TypeCheck(item);
            if (not typeMatch) {
                PyErr_SetString(PyExc_ValueError, "unexpected type of the argument item(s)");
                return 0;
            }
            cppRecSpec->extend(coldpie::FieldSpecification::cppObject(item).get());
            Py_DECREF(item);
        }
        Py_DECREF(iter);

        PyObject* store = coldpie::CppObjectHolder::PyObject_FromCpp(cppRecSpec);
        auto self = coldpie::RecordSpecification::construct(subtype, *cppRecSpec);
        self->owner = store;    // this steals reference
        return self;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return 0;
    }
}

PyObject* _exists(PyObject* self, PyObject* args)
try {
    // parse arguments
    const char* name = nullptr;
    if (not PyArg_ParseTuple(args, "s:RecordSpecification.exists", &name)) {
        return 0;
    }

    auto& rec = coldpie::RecordSpecification::cppObject(self).get();
    return coldpie::cpp2py::convert(rec.exists(name));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

int _contains(PyObject* self, PyObject* arg)
try {

    if (coldpie::FieldSpecification::Object_TypeCheck(arg)) {
        auto& field = coldpie::FieldSpecification::cppObject(arg).get();
        auto& rec = coldpie::RecordSpecification::cppObject(self).get();
        try {
            auto& rec_field = rec[field.name()];
            // also compare types
            return rec_field == field;
        } catch (const cool::RecordSpecificationUnknownField& exc) {
            // no such name
            return 0;
        }
    } else if (const char* name = PyUnicode_AsUTF8(arg)) {
        auto& rec = coldpie::RecordSpecification::cppObject(self).get();
        return rec.exists(name);
    } else {
        // cannot contain any other objects
        return 0;
    }
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return -1;
}

Py_ssize_t _size(PyObject *o)
{
    auto& rec = coldpie::RecordSpecification::cppObject(o).get();
    return rec.size();
}

template <typename Key>
PyObject* _getItemT(PyObject *o, const Key& key, PyObject* exception)
{
    auto& rec = coldpie::RecordSpecification::cppObject(o).get();
    try {
        return coldpie::FieldSpecification::PyObject_FromCpp(rec[key], o);
    } catch (const cool::RecordSpecificationUnknownField& exc) {
        PyErr_SetString(exception, exc.what());
        return 0;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return 0;
    }
}

PyObject* _getMapItem(PyObject *o, PyObject*  key)
{
    // can accept interger or string as a key
    if (PyLong_Check(key)) {
        auto& rec = coldpie::RecordSpecification::cppObject(o).get();
        auto i = PyLong_AsSsize_t(key);
        if (i < 0) i += rec.size();
        if (i < 0 or i >= rec.size()) {
            PyErr_SetString(PyExc_IndexError, "RecordSpecification[]: index out of range");
            return nullptr;
        }
        return _getItemT(o, i, PyExc_IndexError);
    } else if (const char* str = PyUnicode_AsUTF8(key)) {
        std::string keyStr(str);
        return _getItemT(o, keyStr, PyExc_KeyError);
    } else {
        // exception is set by PyUnicode_AsUTF8
        return nullptr;
    }

}

PyObject* _iter(PyObject* self)
try {
    auto& rec = coldpie::RecordSpecification::cppObject(self).get();
    RecordSpecificationIterData iterData(rec);
    return RecordSpecificationIter::PyObject_FromCpp(iterData, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject *_richcompare(PyObject *self, PyObject *other, int op)
{
    if (not coldpie::RecordSpecification::Object_TypeCheck(other)) {
        Py_INCREF(Py_NotImplemented);
        return Py_NotImplemented;
    }
    auto& lhs = coldpie::RecordSpecification::cppObject(self).get();
    auto& rhs = coldpie::RecordSpecification::cppObject(other).get();
    switch (op) {
    case Py_EQ:
        return coldpie::cpp2py::convert(lhs == rhs);
        break;
    case Py_NE:
        return coldpie::cpp2py::convert(lhs != rhs);
        break;
    default:
        break;
    }
    Py_INCREF(Py_NotImplemented);
    return Py_NotImplemented;
}

PyMethodDef methods[] = {
    {"exists", _exists, METH_VARARGS,
        "self.exists(name: str) -> bool\n\n"
        "Return true if given field name exists in the record specification. This is\n"
        "equivalent to `name in self`."},
    {0, 0, 0, 0}};

PySequenceMethods seq_methods = {
    _size, /* lenfunc sq_length */
    0, /* binaryfunc sq_concat */
    0, /* ssizeargfunc sq_repeat */
    0, /* ssizeargfunc sq_item; */
    0, /* ssizessizeargfunc sq_slice; */
    0, /* ssizeobjargproc sq_ass_item; */
    0, /* ssizessizeobjargproc sq_ass_slice; */
    _contains, /* objobjproc sq_contains; */
    0, /* binaryfunc sq_inplace_concat; */
    0  /* ssizeargfunc sq_inplace_repeat; */
};

PyMappingMethods map_methods {
    _size,       /* lenfunc mp_length */
    _getMapItem, /* binaryfunc mp_subscript */
    nullptr      /* objobjargproc mp_ass_subscript */
};

char typedoc[] = "\
RecordSpecification class describes the structure of COOL record. This is a\n\
wrapper for C++ RecordSpecification class and its instances can be passed to\n\
other COOL methods expecting that class. At Python level it is also a sequence\n\
of FieldSpecification instances and can be iterated to access specifications\n\
of individual fields.\n\
\n\
Constructor:\n\
\n\
    RecordSpecification(fields: iterable)\n\
\n\
    Parameters:\n\
        fields : iterable [FieldSpecification]\n\
            Specifications for the fields in records.\n\
\n\
RecordSpecification can be iterated over using regular for construct, e.g.:\n\
\n\
    rec = RecordSpecification([field1, field2])\n\
    for field in rec:\n\
        print(field.name())\n\
\n\
It is possible to check field existence in the record using `in` operator:\n\
\n\
    rec = RecordSpecification([field1, field2])\n\
    if field1 in rec:\n\
        print(field1.name(), \"is in the record\")\n\
\n\
RecordSpecification also implements name-based indexing and testing:\n\
\n\
    # will raise LookupError if \"payload\" filed does not exist\n\
    field = rec[\"payload\"]\n\
    if \"id\" is in rec:\n\
        print(\"id column specification:\", rec[\"id\"])\n\
\n\
Instances of RecordSpecification class can be compared for (non-)equality\n\
using `==` or `!=` operators. Ordering relation is not defined for this\n\
class, less/greater operators do not work on objects.\n\
";

} // namespace

void
coldpie::RecordSpecification::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_new = ::_new;
  type->tp_iter = ::_iter;
  type->tp_as_sequence = &::seq_methods;
  type->tp_as_mapping = &::map_methods;
  type->tp_richcompare = ::_richcompare;

  BaseType::initType("RecordSpecification", module, modname);
  RecordSpecificationIter::initType(module, modname);
}

// Dump object info to a stream
void
coldpie::RecordSpecification::print(std::ostream& out) const
{
  out << "<RecordSpecification@" << this << ">";
}
