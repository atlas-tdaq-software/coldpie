
//-----------------------
// This Class's Header --
//-----------------------
#include "Time.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// __new__
PyObject* _new(PyTypeObject *subtype, PyObject *args, PyObject */*kwds*/)
try {
    cool::Time coolTime(1970, 1, 1, 0, 0, 0, 0);
    try {
        int nargs = PySequence_Size(args);
        if (nargs == 0) {
            // called without arguments - use current time
            coolTime = cool::Time();
        } else if (nargs == 1) {
            // single argument - number of nanoseconds since Epoch
            PY_LONG_LONG nanoseconds;
            if (not PyArg_ParseTuple(args, "L:Time", &nanoseconds)) return 0;
            coolTime = coral::TimeStamp(nanoseconds);
        } else {
            int year, month, day, hour, minute, second;
            long nanosecond = 0;
            if (not PyArg_ParseTuple(args, "iiiiii|l:Time", &year, &month, &day,
                                    &hour, &minute, &second, &nanosecond)) {
                return 0;
            }
            coolTime = cool::Time(year, month, day, hour, minute, second, nanosecond);
        }
    } catch (std::exception const& exc) {
        PyErr_SetString(PyExc_ValueError, exc.what());
        return nullptr;
    }

    auto self = coldpie::Time::construct(subtype, coolTime);
    return self;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}


// type-specific methods
PyObject* _year(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.year());
}

PyObject* _month(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.month());
}

PyObject* _day(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.day());
}

PyObject* _hour(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.hour());
}

PyObject* _minute(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.minute());
}

PyObject* _second(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.second());
}

PyObject* _nanosecond(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.nanosecond());
}

PyObject* _total_nanoseconds(PyObject* self, PyObject*)
{
    auto& time = coldpie::Time::cppObject(self);
    return coldpie::cpp2py::convert(time.coralTimeStamp().total_nanoseconds());
}

long _hash(PyObject* self)
{
    auto& time = coldpie::Time::cppObject(self);
    return long(time.coralTimeStamp().total_nanoseconds());
}

PyObject *_richcompare(PyObject *self, PyObject *other, int op)
{
    if (not coldpie::Time::Object_TypeCheck(other)) {
        PyErr_SetString(PyExc_TypeError, "Cannot compare cool.Time with other object types");
        return nullptr;
    }
    auto& lhs = coldpie::Time::cppObject(self);
    auto& rhs = coldpie::Time::cppObject(other);
    bool result = true;
    switch (op) {
    case Py_LT:
        result = lhs < rhs;
        break;
    case Py_LE:
        result = lhs <= rhs;
        break;
    case Py_EQ:
        result = lhs == rhs;
        break;
    case Py_NE:
        result = lhs != rhs;
        break;
    case Py_GT:
        result = lhs > rhs;
        break;
    case Py_GE:
        result = lhs >= rhs;
        break;
    }
    return coldpie::cpp2py::convert(result);
}

PyMethodDef methods[] = {
    {"year", _year, METH_NOARGS, "self.year() -> int\n\nReturn year."},
    {"month", _month, METH_NOARGS, "self.month() -> int\n\nReturn month as integer in range [1-12]."},
    {"day", _day, METH_NOARGS, "self.day() -> int\n\nReturn day as integer in range [1-31]."},
    {"hour", _hour, METH_NOARGS, "self.hour() -> int\n\nReturn number of hours [0-23]."},
    {"minute", _minute, METH_NOARGS, "self.minute() -> int\n\nReturn number of minutes [0-59]."},
    {"second", _second, METH_NOARGS, "self.second() -> int\n\nReturn number of seconds [0-60]."},
    {"nanosecond", _nanosecond, METH_NOARGS,
        "self.nanosecond() -> int\n\nReturn number of nanoseconds in a second [0-999999999]."},
    {"total_nanoseconds", _total_nanoseconds, METH_NOARGS,
        "self.total_nanoseconds() -> int\n\n"
        "Return total number of nanoseconds passed since Epoch."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Time class holds time in UTC timezone with nanosecond precision.\n\
\n\
Constructors for Time class:\n\
\n\
    Time()\n\
        - makes instance with current time\n\
\n\
    Time(nanoseconds: int)\n\
        - nanoseconds is total nanoseconds passed since Epoch time\n\
\n\
    Time(year: int, month: int, day: int, hour: int, minute: int,\n\
         seconds: int, nsec: int=0)\n\
        - assumes that time is given in UTC\n\
\n\
Time class defines standard set of comparison operators so that instances can\n\
can be ordered in a sensible way. It also defines hash method and instances\n\
can be used as dictionary keys.\n\
";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::Time::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_new = ::_new;
  type->tp_hash = ::_hash;
  type->tp_richcompare = ::_richcompare;

  BaseType::initType("Time", module, modname);
}

// Dump object info to a stream
void
coldpie::Time::print(std::ostream& out) const
{
  out << cpp_obj;
}
