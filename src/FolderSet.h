#ifndef COLDPIE_FOLDERSET_H
#define COLDPIE_FOLDERSET_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PySubclass.h"
#include "HvsNode.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/IFolderSet.h"
#include "CoolKernel/pointers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL IFolderSet(Ptr) class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct FolderSet : public PySubclass<FolderSet, cool::IFolderSetPtr, HvsNode> {

    typedef PySubclass<FolderSet, cool::IFolderSetPtr, HvsNode> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

    // Returns C++ object
    static cool::IFolderSetPtr cppObject(PyObject* self) {
        FolderSet* py_this = static_cast<FolderSet*>(self);
        return std::dynamic_pointer_cast<cool::IFolderSet>(py_this->cpp_obj);
    }

};

} // namespace coldpie

#endif // COLDPIE_FOLDERSET_H
