#ifndef COLDPIE_CHANNELSELECTION_H
#define COLDPIE_CHANNELSELECTION_H

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
#include "CoolKernel/ChannelSelection.h"

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL ChannelSelection class
 */

struct ChannelSelection : public PyDataType<ChannelSelection, cool::ChannelSelection> {

    typedef PyDataType<ChannelSelection, cool::ChannelSelection> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_CHANNELSELECTION_H
