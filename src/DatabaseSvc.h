#ifndef COLDPIE_DATABASESVC_H
#define COLDPIE_DATABASESVC_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <functional>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/IDatabaseSvc.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL IDatabaseSvc class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct DatabaseSvc : public PyDataType<DatabaseSvc, std::reference_wrapper<cool::IDatabaseSvc>> {

    typedef PyDataType<DatabaseSvc, std::reference_wrapper<cool::IDatabaseSvc>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_DATABASESVC_H
