#ifndef COLDPIE_FOLDER_H
#define COLDPIE_FOLDER_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PySubclass.h"
#include "HvsNode.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/IFolder.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL IFolder(Ptr) class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct Folder : public PySubclass<Folder, cool::IFolderPtr, HvsNode> {

    typedef PySubclass<Folder, cool::IFolderPtr, HvsNode> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

    // Returns C++ object
    static cool::IFolderPtr cppObject(PyObject* self) {
        Folder* py_this = static_cast<Folder*>(self);
        return std::dynamic_pointer_cast<cool::IFolder>(py_this->cpp_obj);
    }

};

} // namespace coldpie

#endif // COLDPIE_FOLDER_H
