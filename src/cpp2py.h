#ifndef COLDPIE_CPP2PY_H
#define COLDPIE_CPP2PY_H

//-----------------
// C/C++ Headers --
//-----------------
#include "Python.h"
#include <ctime>
#include <string>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/ITime.h"
#include "Time.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Methods for converting C++ types into Python types.
 *
 *  All conversion methods return new python references.
 */

namespace cpp2py {

inline
PyObject* convert(bool value) {
    PyObject* res = value ? Py_True : Py_False;
    Py_INCREF(res);
    return res;
}

inline
PyObject* convert(short value) {
    return PyLong_FromLong(value);
}

inline
PyObject* convert(unsigned short value) {
    return PyLong_FromLong(value);
}

inline
PyObject* convert(int value) {
    return PyLong_FromLong(value);
}

inline
PyObject* convert(unsigned value) {
    return PyLong_FromLong(value);
}

inline
PyObject* convert(long value) {
    return PyLong_FromLong(value);
}

inline
PyObject* convert(unsigned long value) {
    return PyLong_FromUnsignedLong(value);
}

inline
PyObject* convert(PY_LONG_LONG value) {
    return PyLong_FromLongLong(value);
}

inline
PyObject* convert(unsigned PY_LONG_LONG value) {
    return PyLong_FromUnsignedLongLong(value);
}

inline
PyObject* convert(float value) {
    return PyFloat_FromDouble(double(value));
}

inline
PyObject* convert(double value) {
    return PyFloat_FromDouble(value);
}

inline
PyObject* convert(std::string const& value) {
    return PyUnicode_FromStringAndSize(value.data(), value.size());
}

inline
PyObject* convert(const cool::ITime& itime)
{
    return coldpie::Time::PyObject_FromCpp(itime);
}

template <typename T>
inline
PyObject* convert(std::vector<T> const& vec)
{
    auto pylist = PyList_New(vec.size());
    for (unsigned i = 0; i != vec.size(); ++ i) {
        PyObject* py_item = convert(vec[i]);
        PyList_SET_ITEM(pylist, i, py_item); // steals reference
    }
    return pylist;
}

} // namespace cpp2py
} // namespace coldpie

#endif // COLDPIE_CPP2PY_H
