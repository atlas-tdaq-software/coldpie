#ifndef COLDPIE_HVSNODE_H
#define COLDPIE_HVSNODE_H

#include "coldpie/PyDataType.h"

# include <memory>

#include "CoolKernel/IHvsNode.h"


namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL IHvsNode class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct HvsNode : public PyDataType<HvsNode, std::shared_ptr<cool::IHvsNode>> {

    typedef PyDataType<HvsNode, std::shared_ptr<cool::IHvsNode>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_HvsNode_H
