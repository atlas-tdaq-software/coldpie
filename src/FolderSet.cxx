//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "FolderSet.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/ITime.h"
#include "CoolKernel/Time.h"
#include "coldpie/gil.h"
#include "exceptions.h"
#include "Record.h"
#include "cpp2py.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

PyObject* _listFolders(PyObject* self, PyObject* args)
try {
    int ascending = true;
    if (not PyArg_ParseTuple(args, "|i:FolderSet.listFolders()", &ascending)) {
        return 0;
    }

    auto folderSet = coldpie::FolderSet::cppObject(self);
    std::vector<std::string> folders;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folders = folderSet->listFolders(bool(ascending));
    }
    return coldpie::cpp2py::convert(folders);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _listFolderSets(PyObject* self, PyObject* args)
try {
    int ascending = true;
    if (not PyArg_ParseTuple(args, "|i:FolderSet.listFolderSets()", &ascending)) {
        return 0;
    }

    auto folderSet = coldpie::FolderSet::cppObject(self);
    std::vector<std::string> folders;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folders = folderSet->listFolderSets(bool(ascending));
    }
    return coldpie::cpp2py::convert(folders);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _folderSetAttributes(PyObject* self, PyObject*)
try {
    auto folderSet = coldpie::FolderSet::cppObject(self);
    return coldpie::Record_const::PyObject_FromCpp(folderSet->folderSetAttributes(), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyMethodDef methods[] = {

    // IFolderSet methods
    {"listFolders", _listFolders, METH_VARARGS,
        "self.listFolders(ascending: bool=True) -> list [str]\n\n"
        "Lists all folders at this level in the node hierarchy (ordered alphabetically\n"
        "ascending/descending)."},
    {"listFolderSets", _listFolderSets, METH_VARARGS,
        "self.listFolderSets(ascending:=True) -> list [str]\n\n"
        "Return list of folder sets at this level in the node hierarchy (ordered alphabetically\n"
        "ascending/descending)."},
    {"folderSetAttributes", _folderSetAttributes, METH_NOARGS,
        "self.folderSetAttributes() -> Record\n\n"
        "Return attributes of the folderset (implementation-specific properties not\n"
        "exposed in the API)."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL FolderSet C++ class.\n\
\n\
Instances of this class are created in C++ API, there is no user-accessible\n\
constructor at Python level. Use one of the methods of Database class to make\n\
instances of FolderSet class.\n\
";

} // namespace


//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::FolderSet::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("FolderSet", module, modname);
}

// Dump object info to a stream
void
coldpie::FolderSet::print(std::ostream& out) const
{
    out << "<FolderSet@" << this << ">";
}
