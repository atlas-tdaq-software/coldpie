#ifndef COLDPIE_ENUMS_H
#define COLDPIE_ENUMS_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/EnumType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL enums.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

namespace enums {

EnumType& folderVersioningEnum();
EnumType& hsvTagLockEnum();
EnumType& payloadModeEnum();
EnumType& storageTypeEnum();
EnumType& channelSelectionOrderEnum();  // ChannelSelection::Order

}

} // namespace coldpie

#endif // COLDPIE_ENUMS_H
