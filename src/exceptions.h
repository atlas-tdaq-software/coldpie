#ifndef COLDPIE_EXCEPTIONS_H
#define COLDPIE_EXCEPTIONS_H

//-----------------
// C/C++ Headers --
//-----------------
#include "Python.h"
#include <stdexcept>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Method which sets Python exception when C++ exception happens.
 */
void translateException(const std::exception& exc);

} // namespace coldpie

#endif // COLDPIE_EXCEPTIONS_H
