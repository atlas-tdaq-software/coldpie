//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "DatabaseSvc.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coldpie/gil.h"
#include "cpp2py.h"
#include "exceptions.h"
#include "Database.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

PyObject* _createDatabase(PyObject* self, PyObject* args);
PyObject* _openDatabase(PyObject* self, PyObject* args);
PyObject* _dropDatabase(PyObject* self, PyObject* args);
PyObject* _serviceVersion(PyObject* self, PyObject*);

PyMethodDef methods[] = {
    {"createDatabase", _createDatabase, METH_VARARGS,
        "self.createDatabase(dbId: str) -> Database\n\nCreate database. Returns Database instance."},
    {"openDatabase", _openDatabase, METH_VARARGS,
        "self.openDatabase(dbId: str, readOnly: bool=True) -> Database\n\n"
        "Open database and return Database instance. By default database is open in\n"
        "read-only mode, set second parameter to False to open database in update mode."},
    {"dropDatabase", _dropDatabase, METH_VARARGS,
        "self.dropDatabase(dbId: str) -> bool\n\nDrop database. Returns False if database did not exist."},
    {"serviceVersion", _serviceVersion, METH_NOARGS,
        "self.serviceVersion() -> str\n\nReturn version number of the database service software."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL IDatabaseSvc C++ class.\n\
\n\
Instances of this class are created in C++ API, there is no user-accessible\n\
constructor at Python level. Use DatabaseSvcFactory.databaseService() method\n\
to create instance of this class.\n\
";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------
void
coldpie::DatabaseSvc::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("DatabaseSvc", module, modname);
}

// Dump object info to a stream
void
coldpie::DatabaseSvc::print(std::ostream& out) const
{
  out << "cool.DatabaseSvc()";
}

namespace {

PyObject* _createDatabase(PyObject* self, PyObject* args)
try {
    const char* dbId = 0;
    if (not PyArg_ParseTuple(args, "s:DatabaseSvc.createDatabase", &dbId)) return 0;

    auto dbSvc = coldpie::DatabaseSvc::cppObject(self);
    cool::IDatabasePtr ptr;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        ptr = dbSvc.get().createDatabase(dbId);
    }
    return coldpie::Database::PyObject_FromCpp(ptr);

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _openDatabase(PyObject* self, PyObject* args)
try {
    const char* dbId = 0;
    int readOnly = true;
    if (not PyArg_ParseTuple(args, "s|i:DatabaseSvc.openDatabase", &dbId, &readOnly)) return 0;

    auto dbSvc = coldpie::DatabaseSvc::cppObject(self);
    cool::IDatabasePtr ptr;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        ptr = dbSvc.get().openDatabase(dbId, bool(readOnly));
    }
    return coldpie::Database::PyObject_FromCpp(ptr);

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _dropDatabase(PyObject* self, PyObject* args)
try {
    const char* dbId = 0;
    if (not PyArg_ParseTuple(args, "s:DatabaseSvc.dropDatabase", &dbId)) return 0;

    auto dbSvc = coldpie::DatabaseSvc::cppObject(self);
    bool ok = false;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        ok = dbSvc.get().dropDatabase(dbId);
    }
    PyObject* res = ok ? Py_True : Py_False;
    Py_INCREF(res);
    return res;

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _serviceVersion(PyObject* self, PyObject*)
try {
    auto dbSvc = coldpie::DatabaseSvc::cppObject(self);
    return coldpie::cpp2py::convert(dbSvc.get().serviceVersion());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

}
