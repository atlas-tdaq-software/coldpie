//-----------------------
// This Class's Header --
//-----------------------
#include "HvsNode.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/ITime.h"
#include "CoolKernel/Time.h"
#include "coldpie/gil.h"
#include "exceptions.h"
#include "Record.h"
#include "cpp2py.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

PyObject* _fullPath(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->fullPath());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _description(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->description());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _isLeaf(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->isLeaf());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _isStored(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->isStored());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _insertionTime(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->insertionTime());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _id(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->id());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _parentId(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->parentId());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _setDescription(PyObject* self, PyObject* args)
try {
    const char* description = 0;
    if (not PyArg_ParseTuple(args, "s:HvsNode.setDescription", &description)) return nullptr;
    auto node = coldpie::HvsNode::cppObject(self);
    node->setDescription(description);
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _listTags(PyObject* self, PyObject*)
try {
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->listTags());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _tagInsertionTime(PyObject* self, PyObject* args)
try {
    const char* tag = 0;
    if (not PyArg_ParseTuple(args, "s:HvsNode.tagInsertionTime", &tag)) return nullptr;

    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->tagInsertionTime(tag));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _tagDescription(PyObject* self, PyObject* args)
try {
    const char* tag = 0;
    if (not PyArg_ParseTuple(args, "s:HvsNode.tagDescription", &tag)) return 0;

    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->tagDescription(tag));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _setTagLockStatus(PyObject* self, PyObject* args)
try {
    const char* tag = nullptr;
    int status = 0;
    if (not PyArg_ParseTuple(args, "si:HvsNode.setTagLockStatus", &tag, &status)) {
        return 0;
    }

    auto node = coldpie::HvsNode::cppObject(self);
    node->setTagLockStatus(tag, cool::HvsTagLock::Status(status));
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _tagLockStatus(PyObject* self, PyObject* args)
try {
    const char* tag = 0;
    if (not PyArg_ParseTuple(args, "s:HvsNode.tagLockStatus", &tag)) return 0;

    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->tagLockStatus(tag));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    static bool IHvsNode::isHeadTag( const std::string& tagName )
PyObject* _isHeadTag(PyObject*, PyObject* args)
try {
    const char* tag = 0;
    if (not PyArg_ParseTuple(args, "s:Folder.isHeadTag", &tag)) return 0;
    return coldpie::cpp2py::convert(cool::IHvsNode::isHeadTag(tag));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    static const std::string IHvsNode::headTag()
PyObject* _headTag(PyObject*, PyObject*)
try {
    return coldpie::cpp2py::convert(cool::IHvsNode::headTag());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void IHvsNode::createTagRelation( const std::string& parentTagName,
//                                    const std::string& tagName ) const = 0;
PyObject* _createTagRelation(PyObject* self, PyObject* args)
try {
    const char* parentTag = nullptr;
    const char* tag = nullptr;
    if (not PyArg_ParseTuple(args, "ss:Folder.createTagRelation", &parentTag, &tag)) {
        return 0;
    }

    auto node = coldpie::HvsNode::cppObject(self);
    node->createTagRelation(parentTag, tag);
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void IHvsNode::deleteTagRelation( const std::string& parentTagName ) const = 0;
PyObject* _deleteTagRelation(PyObject* self, PyObject* args)
try {
    const char* parentTag = nullptr;
    if (not PyArg_ParseTuple(args, "s:Folder.deleteTagRelation", &parentTag)) {
        return 0;
    }

    auto node = coldpie::HvsNode::cppObject(self);
    node->deleteTagRelation(parentTag);
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const std::string IHvsNode::findTagRelation( const std::string& parentTagName ) const = 0;
PyObject* _findTagRelation(PyObject* self, PyObject* args)
try {
    const char* parentTag = nullptr;
    if (not PyArg_ParseTuple(args, "s:Folder.findTagRelation", &parentTag)) {
        return 0;
    }
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->findTagRelation(parentTag));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const std::string IHvsNode::resolveTag( const std::string& ancestorTagName ) const = 0;
PyObject* _resolveTag(PyObject* self, PyObject* args)
try {
    const char* ancestorTag = nullptr;
    if (not PyArg_ParseTuple(args, "s:Folder.resolveTag", &ancestorTag)) {
        return 0;
    }
    auto node = coldpie::HvsNode::cppObject(self);
    return coldpie::cpp2py::convert(node->resolveTag(ancestorTag));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void IHvsNode::setTagDescription( const std::string& tagName, const std::string& description ) = 0;
PyObject* _setTagDescription(PyObject* self, PyObject* args)
try {
    const char* tag = nullptr;
    const char* description = nullptr;
    if (not PyArg_ParseTuple(args, "ss:Folder.setTagDescription", &tag, &description)) {
        return 0;
    }

    auto node = coldpie::HvsNode::cppObject(self);
    node->setTagDescription(tag, description);
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyMethodDef methods[] = {
    // Base class IHvsNodeRecord methods
    {"fullPath", _fullPath, METH_NOARGS,
        "self.fullPath() -> str\n\n"
        "Return node full path name in the HVS hierarchy."},
    {"description", _description, METH_NOARGS,
        "self.description() -> str\n\n"
        "Return node description."},
    {"isLeaf", _isLeaf, METH_NOARGS,
        "self.isLeaf() -> bool\n\n"
        "Return True if this is a leaf node (should always return True for Folder)."},
    {"isStored", _isStored, METH_NOARGS,
        "self.isStored() -> bool\n\n"
        "Return True if this node is stored in the database."},
    {"insertionTime", _insertionTime, METH_NOARGS,
        "self.insertionTime() -> Time\n\n"
        "Return insertion time into the database."},
    {"id", _id, METH_NOARGS,
        "self.id() -> int\n\n"
        "Return system-assigned node ID."},
    {"parentId", _parentId, METH_NOARGS,
        "self.parentId() -> int\n\n"
        "Return system-assigned ID of the parent node."},

    // IHvsNode methods
    {"setDescription", _setDescription, METH_VARARGS,
        "self.setDescription(description: str)\n\n"
        "Change the node description stored in the database."},
    {"listTags", _listTags, METH_NOARGS,
        "self.listTags() -> list [str]\n\n"
        "Lists all tags defined for this node (ordered alphabetically).\n\n"
        "Note: The reserved tags \"\" and \"HEAD\" are NOT included in the list."},
    {"tagInsertionTime", _tagInsertionTime, METH_VARARGS,
        "self.tagInsertionTime(tag: str) -> Time\n\n"
        "Return insertion time of a tag defined for this node.\n\n"
        "This is the time when the tag was first assigned to this node."},
    {"tagDescription", _tagDescription, METH_VARARGS,
        "self.tagDescription(tag: str) -> str\n\n"
        "Return description of a tag defined for this node."},
    {"setTagLockStatus", _setTagLockStatus, METH_VARARGS,
        "self.setTagLockStatus(tag: str, status: int)\n\n"
        "Set the persistent lock status of a tag defined for this node.\n\n"
        "Possible status values are defined in HvsTagLock class."},
    {"tagLockStatus", _tagLockStatus, METH_VARARGS,
        "self.tagLockStatus(tag: str) -> int\n\n"
        "Return persistent lock status of a tag defined for this node, possible values\n"
        "are defined in HvsTagLock class."},
    {"isHeadTag", _isHeadTag, METH_VARARGS | METH_STATIC,
        "cls.isHeadTag(tag: str) -> bool [static]\n\n"
        "Check if tag name is a reserved HEAD tag. Returns true for \"\" and any\n"
        "case-insensitive variant of \"HEAD\"."},
    {"headTag", _headTag, METH_NOARGS | METH_STATIC,
        "cls.headTag() -> str [static]\n\n"
        "Most common tag name of the reserved HEAD tag: \"HEAD\"."},
    {"createTagRelation", _createTagRelation, METH_VARARGS,
        "self.createTagRelation(parentTag: str, tag: str)\n\n"
        "Create a relation between a parent node tag and a tag in this node.\n\n"
        "Creates the parent node tag if not defined yet. Creates the tag in this node if not defined yet."},
    {"deleteTagRelation", _deleteTagRelation, METH_VARARGS,
        "self.deleteTagRelation(parentTag: str)\n\n"
        "Delete the relation between a parent tag node and a tag in this node.\n\n"
        "Deletes the parent tag if not related to another parent/child tag. "
        "Deletes the tag in this node if not related to another tag or IOVs."},
    {"findTagRelation", _findTagRelation, METH_VARARGS,
        "self.findTagRelation(parentTag: str) -> str\n\n"
        "Return the tag in this node associated to the given parent node tag."},
    {"resolveTag", _resolveTag, METH_VARARGS,
        "self.resolveTag(ancestorTag: str) -> str\n\n"
        "Main HVS method: determine the tag in this node that is related to the\n"
        "given ancestor tag (assumed to be defined in an ancestor of this node)."},
    {"setTagDescription", _setTagDescription, METH_VARARGS,
        "self.setTagDescription(tag: str, description: str)\n\n"
        "Set the description of a tag."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL IHvsNode C++ class.\n\
\n\
Instances of this class are created in C++ API, there is no user-accessible\n\
constructor at Python level. Use one of the methods of Database class to make\n\
instances of HvsNode class.\n\
";

} // namespace


//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::HvsNode::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("HvsNode", module, modname);
}

// Dump object info to a stream
void
coldpie::HvsNode::print(std::ostream& out) const
{
    out << "<HvsNode@" << this << ">";
}
