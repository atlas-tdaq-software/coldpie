#ifndef COLDPIE_DATABASE_H
#define COLDPIE_DATABASE_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/IDatabase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL IDatabase(Ptr) class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct Database : public PyDataType<Database, cool::IDatabasePtr> {

    typedef PyDataType<Database, cool::IDatabasePtr> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_DATABASE_H
