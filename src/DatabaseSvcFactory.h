#ifndef COLDPIE_DATABASESVCFACTORY_H
#define COLDPIE_DATABASESVCFACTORY_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolApplication/DatabaseSvcFactory.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL DatabaseSvcFactory class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct DatabaseSvcFactory : PyDataType<DatabaseSvcFactory, cool::DatabaseSvcFactory> {

    typedef PyDataType<DatabaseSvcFactory, cool::DatabaseSvcFactory> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_DATABASESVCFACTORY_H
