#ifndef COLDPIE_FIELD_H
#define COLDPIE_FIELD_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <functional>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/IField.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for IField class (const and non-const)
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct Field : public PyDataType<Field, std::reference_wrapper<cool::IField>> {

    typedef PyDataType<Field, std::reference_wrapper<cool::IField>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Set field value from Python object, returns 0 on success, -1 on failure
    // and sets exception.
    static int setValue(cool::IField& field, PyObject* value);

    // Get field value from Python object. Owner pointer is a Python object which
    // owns the C++ field (directly or indirectly).
    static PyObject* data(const cool::IField& field, PyObject* owner);

    // Set field value from Python object, returns 0 on success, -1 on failure
    // and sets exception.
    int setValue(PyObject* value);

    // Get field value from Python object.
    PyObject* data();

    // Dump object info to a stream
    void print(std::ostream& out) const;
};

struct Field_const : public PyDataType<Field, std::reference_wrapper<const cool::IField>> {

    typedef PyDataType<Field, std::reference_wrapper<const cool::IField>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Get field value from Python object. Owner pointer is a Python object which
    // owns the C++ field (directly or indirectly).
    static PyObject* data(const cool::IField& field, PyObject* owner);

    // Get field value from Python object.
    PyObject* data();

    // Dump object info to a stream
    void print(std::ostream& out) const;
};

} // namespace coldpie

#endif // COLDPIE_FIELD_H
