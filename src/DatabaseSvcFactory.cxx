//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "DatabaseSvcFactory.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "DatabaseSvc.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

PyObject* _databaseService(PyObject* self, PyObject*);

PyMethodDef methods[] = {
    {"databaseService", _databaseService, METH_NOARGS|METH_STATIC,
        "DatabaseSvcFactory.databaseService() -> DatabaseSvc\n\n"
        "(staticmethod) Return database service instance."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL DatabaseSvcFactory C++ class. This class defines\n\
one static method databaseService() which creates instances of DatabaseSvc.\n\
";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::DatabaseSvcFactory::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("DatabaseSvcFactory", module, modname);
}

// Dump object info to a stream
void
coldpie::DatabaseSvcFactory::print(std::ostream& out) const
{
  out << "cool.DatabaseSvcFactory()";
}

namespace {

PyObject* _databaseService(PyObject* self, PyObject*)
try {
    return coldpie::DatabaseSvc::PyObject_FromCpp(std::ref(cool::DatabaseSvcFactory::databaseService()), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

}
