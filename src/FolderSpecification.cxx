//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "FolderSpecification.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/FolderSpecification.h"
#include "cpp2py.h"
#include "CppObjectHolder.h"
#include "RecordSpecification.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// standard Python stuff
PyObject* _new(PyTypeObject *subtype, PyObject *args, PyObject* /*kwds*/)
{
    // parse arguments
    int versioning = 0;
    PyTypeObject* specType = coldpie::RecordSpecification::typeObject();
    PyObject* recspec = nullptr;
    int payload = cool::PayloadMode::INLINEPAYLOAD;
    if (not PyArg_ParseTuple(args, "iO!|i:FolderSpecification", &versioning, specType, &recspec, &payload)) {
        return 0;
    }
    const cool::IRecordSpecification& rspec = coldpie::RecordSpecification::cppObject(recspec).get();

    // construct C++ sub-object
    auto versioningEnum = static_cast<cool::FolderVersioning::Mode>(versioning);
    auto payloadEnum = static_cast<cool::PayloadMode::Mode>(payload);
    try {
        auto cppSpec = std::make_shared<cool::FolderSpecification>(versioningEnum, rspec, payloadEnum);
        PyObject* store = coldpie::CppObjectHolder::PyObject_FromCpp(cppSpec);
        auto self = coldpie::FolderSpecification::construct(subtype, *cppSpec);
        self->owner = store;    // this steals reference
        return self;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return 0;
    }

}

PyObject* _versioningMode(PyObject* self, PyObject*)
try {
    auto& fs = coldpie::FolderSpecification::cppObject(self).get();
    return coldpie::cpp2py::convert(int(fs.versioningMode()));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _payloadMode(PyObject* self, PyObject*)
try {
    auto& fs = coldpie::FolderSpecification::cppObject(self).get();
    return coldpie::cpp2py::convert(int(fs.payloadMode()));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _payloadSpecification(PyObject* self, PyObject*)
try {
    auto& fs = coldpie::FolderSpecification::cppObject(self).get();
    return coldpie::RecordSpecification::PyObject_FromCpp(fs.payloadSpecification(), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject *_richcompare(PyObject *self, PyObject *other, int op)
{
    if (not coldpie::FolderSpecification::Object_TypeCheck(other)) {
        Py_INCREF(Py_NotImplemented);
        return Py_NotImplemented;
    }
    auto& lhs = coldpie::FolderSpecification::cppObject(self).get();
    auto& rhs = coldpie::FolderSpecification::cppObject(other).get();
    switch (op) {
    case Py_EQ:
        return coldpie::cpp2py::convert(lhs == rhs);
        break;
    case Py_NE:
        return coldpie::cpp2py::convert(lhs != rhs);
        break;
    default:
        break;
    }
    Py_INCREF(Py_NotImplemented);
    return Py_NotImplemented;
}

PyMethodDef methods[] = {
    {"payloadMode", _payloadMode, METH_NOARGS,
        "self.payloadMode() -> int\n\n"
        "Return payload mode, possible values are defined in PayloadMode."},
    {"versioningMode", _versioningMode, METH_NOARGS,
        "self.versioningMode() -> int\n\n"
        "Return versioning mode, possible values are defined in FolderVersioning."},
    {"payloadSpecification", _payloadSpecification, METH_NOARGS,
        "self.payloadSpecification() -> RecordSpecification\n\n"
        "Return RecordSpecification describing folder payload."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
FolderSpecification class describes folder payload.\n\
\n\
Constructor:\n\
\n\
    FolderSpecification(versioningMode: int, payloadSpec: RecordSpecification,\n\
                        payloadMode: int=PayloadMode.INLINEPAYLOAD)\n\
\n\
    Parameters:\n\
        versioningMode : int\n\
            One of the constants defined in FolderVersioning enum.\n\
        payloadSpec : RecordSpecification\n\
            Specification for payload records.\n\
        payloadMode : int, optional\n\
            One of the constants defined in PayloadMode enum.\n\
\n\
Instances of FolderSpecification class can be compared for (non-)equality\n\
using `==` or `!=` operators. Ordering relation is not defined for this\n\
class, less/greater operators do not work on objects.\n\
";

}

void
coldpie::FolderSpecification::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_new = ::_new;
  type->tp_richcompare = ::_richcompare;

  BaseType::initType("FolderSpecification", module, modname);
}

// Dump object info to a stream
void
coldpie::FolderSpecification::print(std::ostream& out) const
{
  out << "<FolderSpecification@" << this << ">";
}
