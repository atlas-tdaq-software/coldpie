//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "ObjectIterator.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "Object.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

PyObject* _iter(PyObject* o);
PyObject* _iternext(PyObject* o);

char typedoc[] = "\
Python wrapper for C++ ObjectIterator type, it behaves like a regular Python\n\
iterator type.\
";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::ObjectIterator::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_iter = _iter;
  type->tp_iternext = _iternext;

  BaseType::initType("_ObjectIterator", module, modname);
}

// Dump object info to a stream
void
coldpie::ObjectIterator::print(std::ostream& out) const
{
  out << "ObjectIterator(" << cpp_obj.get() << ")";
}

namespace {

PyObject* _iter(PyObject* self)
{
    Py_INCREF(self);
    return self;
}

PyObject* _iternext(PyObject* self)
try {
    auto& oiter = coldpie::ObjectIterator::cppObject(self);
    if (not oiter->isEmpty()) {
        if (oiter->goToNext()) {
            cool::IObject& iobj = const_cast<cool::IObject&>(oiter->currentRef());
            // here is tricky part - iterator apparently holds the ownership
            // of the Object and only gives us reference to it. coldptr::Object
            // wrapper wants shared pointer to Ibject. We could call clone()
            // to make new instance but that means making a copy whch I want
            // to avoid. Instead I'm going to make a non-owning shared pointer
            // which is OK, because we manage owhership using Python tools
            // (lifetime of returned Object is tied to lifetime of self).
            std::shared_ptr<cool::IObject> iobj_ptr(&iobj, [](cool::IObject*) {});
            return coldpie::Object::PyObject_FromCpp(std::move(iobj_ptr), self);
        }
    }

    // signal end of iteration
    PyErr_SetString(PyExc_StopIteration, "end of iteration");
    return nullptr;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

}
