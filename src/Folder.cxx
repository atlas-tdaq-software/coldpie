//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "Folder.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coldpie/gil.h"
#include "coldpie/make_pyshared.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolKernel/RecordSpecification.h"
#include "CoolKernel/Time.h"
#include "ChannelSelection.h"
#include "FolderSpecification.h"
#include "Object.h"
#include "ObjectIterator.h"
#include "Record.h"
#include "RecordSpecification.h"
#include "Time.h"
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

//    virtual const IFolderSpecification& folderSpecification() const = 0;
PyObject* _folderSpecification(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    const auto& fspec = folder->folderSpecification();
    return coldpie::FolderSpecification::PyObject_FromCpp(fspec, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const IRecordSpecification& payloadSpecification() const = 0;
PyObject* _payloadSpecification(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    return coldpie::RecordSpecification::PyObject_FromCpp(folder->payloadSpecification(), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual FolderVersioning::Mode versioningMode() const = 0;
PyObject* _versioningMode(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    return PyLong_FromLong(folder->versioningMode());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const IRecord& folderAttributes() const = 0;
PyObject* _folderAttributes(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    const auto& attr = folder->folderAttributes();
    return coldpie::Record_const::PyObject_FromCpp(attr, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void flushStorageBuffer() = 0;
PyObject* _flushStorageBuffer(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->flushStorageBuffer();
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void setupStorageBuffer( bool useBuffer = true ) = 0;
PyObject* _setupStorageBuffer(PyObject* self, PyObject* args)
try {
    int useBuffer = true;
    if (not PyArg_ParseTuple(args, "|i:Folder.setupStorageBuffer()", &useBuffer)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    folder->setupStorageBuffer(bool(useBuffer));
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual void storeObject( const ValidityKey& since,
//                           const ValidityKey& until,
//                           const IRecord& payload,
//                           const ChannelId& channelId,
//                           const std::string& userTagName = "",
//                           bool userTagOnly = false ) = 0;
PyObject* _storeObject(PyObject* self, PyObject* args)
try {
    unsigned PY_LONG_LONG since, until;
    PyObject* pyRec = nullptr;
    int channelId;
    const char* tagName = "";
    int userTagOnly = 0;
    if (not PyArg_ParseTuple(args, "KKOi|si:Folder.storeObject", &since, &until, &pyRec,
                             &channelId, &tagName, &userTagOnly)) return 0;

    // get c++ record
    const cool::IRecord* rec = nullptr;
    if (coldpie::Record::Object_TypeCheck(pyRec)) {
        rec = &coldpie::Record::cppObject(pyRec).get();
    } else if (coldpie::Record_const::Object_TypeCheck(pyRec)) {
        rec = &coldpie::Record_const::cppObject(pyRec).get();
    } else {
        PyErr_Format(PyExc_TypeError,
                     "Folder.storeObject() argument must be Record or Record_const, not %s",
                     pyRec->ob_type->tp_name);
        return nullptr;
    }
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        auto folder = coldpie::Folder::cppObject(self);
        folder->storeObject(since, until, *rec, channelId, tagName, bool(userTagOnly));
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual int truncateObjectValidity( const ValidityKey& until,
//                                        const ChannelSelection& channels ) = 0;
PyObject* _truncateObjectValidity(PyObject* self, PyObject* args)
try {
    unsigned PY_LONG_LONG until;
    auto chSelType = coldpie::ChannelSelection::typeObject();
    PyObject* pyChSel = nullptr;
    if (not PyArg_ParseTuple(args, "KO!:Folder.findObjects", &until, chSelType, &pyChSel)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    auto& chSel = coldpie::ChannelSelection::cppObject(pyChSel);
    return coldpie::cpp2py::convert(folder->truncateObjectValidity(until, chSel));
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual IObjectPtr findObject( const ValidityKey& pointInTime,
//                                const ChannelId& channelId,
//                                const std::string& tagName="" ) const = 0;
PyObject* _findObject(PyObject* self, PyObject* args)
try {
    unsigned PY_LONG_LONG key;
    int channelId;
    const char* tagName = "";
    if (not PyArg_ParseTuple(args, "Ki|s:Folder.findObject", &key, &channelId, &tagName)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    // release GIL for possibly long I/O operation
    cool::IObjectPtr obj;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        obj = folder->findObject(key, channelId, tagName);
    }
    return coldpie::Object::PyObject_FromCpp(obj);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual IObjectIteratorPtr
// findObjects( const ValidityKey& pointInTime,
//              const ChannelSelection& channels,
//              const std::string& tagName = "" ) const = 0;
PyObject* _findObjects(PyObject* self, PyObject* args)
try {
    unsigned PY_LONG_LONG key;
    auto chSelType = coldpie::ChannelSelection::typeObject();
    PyObject* pyChSel = nullptr;
    const char* tagName = "";
    if (not PyArg_ParseTuple(args, "KO!|s:Folder.findObjects", &key, chSelType, &pyChSel, &tagName)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    auto& chSel = coldpie::ChannelSelection::cppObject(pyChSel);
    cool::IObjectIteratorPtr itr;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        itr = folder->findObjects(key, chSel, tagName);
    }
    return coldpie::ObjectIterator::PyObject_FromCpp(itr, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual IObjectIteratorPtr
// browseObjects( const ValidityKey& since,
//                const ValidityKey& until,
//                const ChannelSelection& channels,
//                const std::string& tagName = "",
//                const IRecordSelection* payloadQuery = 0 ) const = 0;
PyObject* _browseObjects(PyObject* self, PyObject* args)
try {
    unsigned PY_LONG_LONG since, until;
    auto chSelType = coldpie::ChannelSelection::typeObject();
    PyObject* pyChSel = nullptr;
    const char* tagName = "";
    if (not PyArg_ParseTuple(args, "KKO!|s:Folder.browseObjects", &since, &until, chSelType, &pyChSel, &tagName)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    auto& chSel = coldpie::ChannelSelection::cppObject(pyChSel);
    cool::IObjectIteratorPtr itr;
    {
        coldpie::gil gil;
        itr = folder->browseObjects(since, until, chSel, tagName);
    }
    return coldpie::ObjectIterator::PyObject_FromCpp(itr, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual unsigned int
// countObjects( const ValidityKey& since,
//               const ValidityKey& until,
//               const ChannelSelection& channels,
//               const std::string& tagName = "",
//               const IRecordSelection* payloadQuery = 0 ) const = 0;
PyObject* _countObjects(PyObject* self, PyObject* args)
try {
    unsigned PY_LONG_LONG since, until;
    auto chSelType = coldpie::ChannelSelection::typeObject();
    PyObject* pyChSel = nullptr;
    const char* tagName = "";
    if (not PyArg_ParseTuple(args, "KKO!|s:Folder.countObjects", &since, &until, chSelType, &pyChSel, &tagName)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    auto& chSel = coldpie::ChannelSelection::cppObject(pyChSel);
    unsigned count = 0;
    {
        coldpie::gil gil;
        count = folder->countObjects(since, until, chSel, tagName);
    }
    return coldpie::cpp2py::convert(count);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void setPrefetchAll( bool prefetchAll ) = 0;
PyObject* _setPrefetchAll(PyObject* self, PyObject* args)
try {
    int prefetchAll = true;
    if (not PyArg_ParseTuple(args, "|i:Folder.setPrefetchAll()", &prefetchAll)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    folder->setPrefetchAll(bool(prefetchAll));
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void renamePayload( const std::string& oldName, const std::string& newName ) = 0;
PyObject* _renamePayload(PyObject* self, PyObject* args)
try {
    const char* oldName = nullptr;
    const char* newName = nullptr;
    if (not PyArg_ParseTuple(args, "ss:Folder.renamePayload", &oldName, &newName)) {
        return 0;
    }

    auto folder = coldpie::Folder::cppObject(self);
    folder->renamePayload(oldName, newName);
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void extendPayloadSpecification( const IRecord& record ) = 0;
PyObject* _extendPayloadSpecification(PyObject* self, PyObject* args)
try {
    PyObject* pyRec = nullptr;
    if (not PyArg_ParseTuple(args, "O:Folder.extendPayloadSpecification", &pyRec)) return 0;

    // get c++ record
    const cool::IRecord* rec = nullptr;
    if (coldpie::Record::Object_TypeCheck(pyRec)) {
        rec = &coldpie::Record::cppObject(pyRec).get();
    } else if (coldpie::Record_const::Object_TypeCheck(pyRec)) {
        rec = &coldpie::Record_const::cppObject(pyRec).get();
    } else {
        PyErr_Format(PyExc_TypeError,
                     "Folder.extendPayloadSpecification() argument must be Record or Record_const, not %s",
                     pyRec->ob_type->tp_name);
        return nullptr;
    }

    auto folder = coldpie::Folder::cppObject(self);
    folder->extendPayloadSpecification(*rec);
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void tagCurrentHead( const std::string& tagName, const std::string& description = "" ) const = 0;
PyObject* _tagCurrentHead(PyObject* self, PyObject* args)
try {
    const char* name = nullptr;
    const char* descr = "";
    if (not PyArg_ParseTuple(args, "i|s:Folder.tagCurrentHead", &name, &descr)) return nullptr;

    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->tagCurrentHead(name, descr);
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void cloneTagAsUserTag( const std::string& tagName, const std::string& tagClone,
//                       const std::string& description = "", bool forceOverwriteTag = false ) = 0;
PyObject* _cloneTagAsUserTag(PyObject* self, PyObject* args)
try {
    const char* tagName = nullptr;
    const char* tagClone = nullptr;
    const char* descr = "";
    int forceOverwriteTag = 0;
    if (not PyArg_ParseTuple(args, "ss|is:Folder.cloneTagAsUserTag",
                             &tagName, &tagClone, &descr, &forceOverwriteTag)) return nullptr;

    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->cloneTagAsUserTag(tagName, tagClone, descr, bool(forceOverwriteTag));
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void tagHeadAsOfDate( const ITime& asOfDate, const std::string& tagName,
//                     const std::string& description = "" ) const = 0;
PyObject* _tagHeadAsOfDate(PyObject* self, PyObject* args)
try {
    PyObject* pyTime = nullptr;
    const char* tagName = nullptr;
    const char* descr = "";
    if (not PyArg_ParseTuple(args, "O!s|s:Folder.tagHeadAsOfDate",
                             coldpie::Time::typeObject(), &pyTime, &tagName, &descr)) return nullptr;

    auto& asOfDate = coldpie::Time::cppObject(pyTime);
    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->tagHeadAsOfDate(asOfDate, tagName, descr);
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const Time insertionTimeOfLastObjectInTag( const std::string& tagName ) const = 0;
PyObject* _insertionTimeOfLastObjectInTag(PyObject* self, PyObject* args)
try {
    const char* tagName = nullptr;
    if (not PyArg_ParseTuple(args, "s:Folder.insertionTimeOfLastObjectInTag", &tagName)) return nullptr;

    auto folder = coldpie::Folder::cppObject(self);
    cool::Time time;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        time = folder->insertionTimeOfLastObjectInTag(tagName);
    }
    return coldpie::cpp2py::convert(time);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual void deleteTag( const std::string& tagName ) = 0;
PyObject* _deleteTag(PyObject* self, PyObject* args)
try {
    const char* tagName = nullptr;
    if (not PyArg_ParseTuple(args, "s:Folder.deleteTag", &tagName)) return nullptr;

    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->deleteTag(tagName);
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual bool existsUserTag( const std::string& userTagName ) const = 0;
PyObject* _existsUserTag(PyObject* self, PyObject* args)
try {
    const char* tagName = nullptr;
    if (not PyArg_ParseTuple(args, "s:Folder.existsUserTag", &tagName)) return nullptr;

    auto folder = coldpie::Folder::cppObject(self);
    bool exists;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        exists = folder->existsUserTag(tagName);
    }
    return coldpie::cpp2py::convert(exists);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const std::vector<ChannelId> listChannels() const = 0;
PyObject* _listChannels(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    return coldpie::cpp2py::convert(folder->listChannels());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const std::map<ChannelId,std::string> listChannelsWithNames() const = 0;
PyObject* _listChannelsWithNames(PyObject* self, PyObject*)
try {
    auto folder = coldpie::Folder::cppObject(self);
    auto channelMap = folder->listChannelsWithNames();
    PyObject* dict = PyDict_New();
    for (auto&& pair: channelMap) {
        auto key = coldpie::make_pyshared(coldpie::cpp2py::convert(pair.first));
        auto val = coldpie::make_pyshared(coldpie::cpp2py::convert(pair.second));
        PyDict_SetItem(dict, key.get(), val.get());
    }
    return dict;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual void createChannel( const ChannelId& channelId,
//                             const std::string& channelName,
//                             const std::string& description = "" ) = 0;
PyObject* _createChannel(PyObject* self, PyObject* args)
try {
    int channelId;
    const char* name;
    const char* descr = "";
    if (not PyArg_ParseTuple(args, "is|s:Folder.createChannel", &channelId, &name, &descr)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->createChannel(channelId, name, descr);
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual bool dropChannel( const ChannelId& channelId ) = 0;
PyObject* _dropChannel(PyObject* self, PyObject* args)
try {
    int channelId;
    if (not PyArg_ParseTuple(args, "i:Folder.dropChannel", &channelId)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    bool dropped;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        dropped = folder->dropChannel(channelId);
    }
    return coldpie::cpp2py::convert(dropped);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//     virtual void setChannelName( const ChannelId& channelId, const std::string& channelName ) = 0;
PyObject* _setChannelName(PyObject* self, PyObject* args)
try {
    int channelId;
    const char* name;
    if (not PyArg_ParseTuple(args, "is:Folder.setChannelName", &channelId, &name)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->setChannelName(channelId, name);
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//     virtual const std::string channelName( const ChannelId& channelId ) const = 0;
PyObject* _channelName(PyObject* self, PyObject* args)
try {
    int channelId;
    if (not PyArg_ParseTuple(args, "i:Folder.channelName", &channelId)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    std::string name;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        name = folder->channelName(channelId);
    }
    return coldpie::cpp2py::convert(name);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual ChannelId channelId( const std::string& channelName ) const = 0;
PyObject* _channelId(PyObject* self, PyObject* args)
try {
    const char* name;
    if (not PyArg_ParseTuple(args, "s:Folder.channelId", &name)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    cool::ChannelId id = 0;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        id = folder->channelId(name);
    }
    return coldpie::cpp2py::convert(id);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual bool existsChannel( const std::string& channelName ) const = 0;
//    virtual bool existsChannel( const ChannelId& channelId ) const = 0;
PyObject* _existsChannel(PyObject* self, PyObject* args)
try {
    PyObject* arg;
    if (not PyArg_UnpackTuple(args, "Folder.existsChannel", 1, 1, &arg)) return 0;

    auto folder = coldpie::Folder::cppObject(self);

    bool exists = false;
    if (PyUnicode_Check(arg)) {
        // string overload
        const char* name = PyUnicode_AsUTF8(arg);
        {
            coldpie::gil gil;
            exists = folder->existsChannel(name);
        }
    } else if (PyLong_Check(arg)) {
        cool::ChannelId channelId = PyLong_AsUnsignedLong(arg);
        {
            coldpie::gil gil;
            exists = folder->existsChannel(channelId);
        }
    } else {
        PyErr_Format(PyExc_TypeError,
                     "Folder.existsChannel() argument must be int or string, not %s",
                     arg->ob_type->tp_name);
    }
    return coldpie::cpp2py::convert(exists);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual void setChannelDescription( const ChannelId& channelId,
//                                     const std::string& description ) = 0;
PyObject* _setChannelDescription(PyObject* self, PyObject* args)
try {
    int channelId;
    const char* desc;
    if (not PyArg_ParseTuple(args, "is:Folder.setChannelDescription", &channelId, &desc)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        folder->setChannelDescription(channelId, desc);
    }
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

// virtual const std::string
// channelDescription( const ChannelId& channelId ) const = 0;
PyObject* _channelDescription(PyObject* self, PyObject* args)
try {
    int channelId;
    if (not PyArg_ParseTuple(args, "i:Folder.channelDescription", &channelId)) return 0;

    auto folder = coldpie::Folder::cppObject(self);
    std::string descr;
    {
        // release GIL for possibly long I/O operation
        coldpie::gil gil;
        descr = folder->channelDescription(channelId);
    }
    return coldpie::cpp2py::convert(descr);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}


PyMethodDef methods[] = {

    // IFolder methods
    {"folderSpecification", _folderSpecification, METH_NOARGS,
        "self.folderSpecification() -> FolderSpecification\n\n"
        "Return FolderSpecification instance for this folder."},
    {"payloadSpecification", _payloadSpecification, METH_NOARGS,
        "self.payloadSpecification() -> RecordSpecification\n\n"
        "Return RecordSpecification instance for folder payload."},
    {"versioningMode", _versioningMode, METH_NOARGS,
        "self.versioningMode() -> int\n\n"
        "Return the folder versioning mode, values are defined in FolderVersioning class."},
    {"folderAttributes", _folderAttributes, METH_NOARGS,
        "self.folderAttributes() -> Record\n\n"
        "Return attributes of the folder as Record instance."},
    {"setupStorageBuffer", _setupStorageBuffer, METH_VARARGS,
        "self.setupStorageBuffer(useBuffer: bool)\n\n"
        "Activate (with True argument) or deactivate a storage buffer for bulk\n"
        "insertion of objects."},
    {"flushStorageBuffer", _flushStorageBuffer, METH_NOARGS,
        "self.flushStorageBuffer()\n\nFlush storage buffers."},
    {"storeObject", _storeObject, METH_VARARGS,
        "self.storeObject(since: int, until: int, payload: Record, channelId: int,\n"
        "                 userTagName: str='', userTagOnly: bool=False)\n\n"
        "Store an object in a given channel with the given IOV and data payload.\n"
        "`since` and `until` define start and end of IOV, these are 64-bit integers.\n"
        "Channel has to be specified as a number (ID) and not channel name."
        },
    {"truncateObjectValidity", _truncateObjectValidity, METH_VARARGS,
        "self.truncateObjectValidity(until: int, channels: ChannelSelection) -> int\n\n"
        "Set a new finite end-of-validity value for all SV objects in a given\n"
        "channel selection whose end-of-validity is currently infinite.\n\n"
        "The channel selection is specified through a ChannelSelection object.\n"
        "Returns the number of actually truncated IOVs."},
    {"findObject", _findObject, METH_VARARGS,
        "self.findObject(pointInTime: int, channelId: int, tagName: str='') -> Object\n\n"
        "Find and return one object in a given channel valid at the given point in time.\n"
        "Note: This method returns cool.Object instance (not general Python object)."},
    {"findObjects", _findObjects, METH_VARARGS,
        "self.findObjects(pointInTime: int, channels: ChannelSelection,\n"
        "                 tagName: str='') -> iterator [Object]\n\n"
        "Find the objects for a given channel selection at the given point in time\n"
        "in the given tag. This method returns iterator for Object instances."},
    {"browseObjects", _browseObjects, METH_VARARGS,
        "self.browseObjects(since: int, until: int, channels: ChannelSelection,\n"
        "                   tagName: str='') -> iterator [Object]\n\n"
        "Browse the objects for a given channel selection within the given time range\n"
        "and the given tag. This method returns iterator for Object instances."},
    {"countObjects", _countObjects, METH_VARARGS,
        "self.countObjects(since: int, until: int, channels: ChannelSelection,\n"
        "                  tagName: str='') -> int\n\n"
        "Count the number of objects that would be returned by the browseObjects\n"
        "method for the same selection parameters."},
    {"setPrefetchAll", _setPrefetchAll, METH_VARARGS,
        "self.setPrefetchAll(prefetchAll: bool)\n\n"
        "Change the object prefetch policy (default is prefetchAll=true)."},
    {"renamePayload", _renamePayload, METH_VARARGS,
        "self.renamePayload(oldName: str, newName: str)\n\n"
        "Rename a payload item."},
    {"extendPayloadSpecification", _extendPayloadSpecification, METH_VARARGS,
        "self.extendPayloadSpecification(record: Record)\n\n"
        "Rename a payload item."},
    {"tagCurrentHead", _tagCurrentHead, METH_VARARGS,
        "self.tagCurrentHead(tagName: str, description: str='')\n\n"
        "Associate the objects that are the current HEAD with the given tag name\n"
        "and tag description."},
    {"cloneTagAsUserTag", _cloneTagAsUserTag, METH_VARARGS,
        "self.cloneTagAsUserTag(tagName: str, tagClone: str, description: str='',\n"
        "                       forceOverwriteTag: bool=False)\n\n"
        "Clone the tag `tagName` as user tag `tagClone` by reinserting the IOVs\n"
        "with the user tag. Does not modify the current head."},
    {"tagHeadAsOfDate", _tagHeadAsOfDate, METH_VARARGS,
        "self.tagHeadAsOfDate(asOfDate: Time, tagName: str, description: str='')\n\n"
        "Associate the objects that were the HEAD at 'asOfDate' with the given\n"
        "tag name and tag description."},
    {"insertionTimeOfLastObjectInTag", _insertionTimeOfLastObjectInTag, METH_VARARGS,
        "self.insertionTimeOfLastObjectInTag(tagName: str) -> Time\n\n"
        "Return insertion time of the last inserted IOV in a tag defined for this node.\n\n"
        "This represents the first possible tagging date for the tag (for tagging via\n"
        "tagCurrentHead or tagHeadAsOfDate), i.e. the first date at which the IOVs\n"
        "in this tag were the HEAD."},
    {"deleteTag", _deleteTag, METH_NOARGS,
        "self.deleteTag(tagName: str)\n\n"
        "Delete a tag from this folder and from the global \"tag namespace\": the tag\n"
        "name is available for tagging again afterwards."},
    {"existsUserTag", _existsUserTag, METH_NOARGS,
        "self.existsUserTag(userTagName: str) -> bool\n\n"
        "Return True if user tag exists."},
    {"listChannels", _listChannels, METH_NOARGS,
        "self.listChannels() -> list [int]\n\n"
        "Return the list of existing channels IDs (ordered by ascending channelId)."},
    {"listChannelsWithNames", _listChannelsWithNames, METH_NOARGS,
        "self.listChannelsWithNames() -> dict [int, str]\n\n"
        "Return the map of id->name for all existing channels."},
    {"createChannel", _createChannel, METH_VARARGS,
        "self.createChannel(channelId: int, channelName: str, description: str='')\n\n"
        "Create a new channel with the given id, name and description."},
    {"dropChannel", _dropChannel, METH_VARARGS,
        "self.dropChannel(channelId: int) -> bool\n\n"
        "Drop (delete) an existing channel, given its id.\n\n"
        "Return True if the channel is dropped as expected. Return False (without\n"
        "throwing any exception) if the channel does not exist any more on exit\n"
        "from this method, but it did not exist to start with."},
    {"setChannelName", _setChannelName, METH_VARARGS,
        "self.setChannelName(channelId: int, channelName: str)\n\n"
        "Set the name of a channel, given its id."},
    {"channelName", _channelName, METH_VARARGS,
        "self.channelName(channelId: int) -> str\n\n"
        "Return the name of a channel given its channel id."},
    {"channelId", _channelId, METH_VARARGS,
        "self.channelId(channelName: str) -> int\n\n"
        "Return the id of a channel given its name."},
    {"existsChannel", _existsChannel, METH_VARARGS,
        "self.existsChannel(channelId: int) -> bool\n\n"
        "Return True if a channel with given id exists."},
    {"setChannelDescription", _setChannelDescription, METH_VARARGS,
        "self.setChannelDescription(channelId: int, description: str)\n\n"
        "Set the description of a channel, given its id."},
    {"channelDescription", _channelDescription, METH_VARARGS,
        "self.channelDescription(channelId: int) -> str\n\n"
        "Return the description of a channel given its id."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL Folder C++ class.\n\
\n\
Instances of this class are created in C++ API, there is no user-accessible\n\
constructor at Python level. Use one of the methods of Database class to make\n\
instances of Folder class.\n\
";

}

void
coldpie::Folder::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("Folder", module, modname);
}

// Dump object info to a stream
void
coldpie::Folder::print(std::ostream& out) const
{
    out << "<Folder@" << this << ">";
}
