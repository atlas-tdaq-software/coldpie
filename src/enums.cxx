//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "enums.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/ChannelSelection.h"
#include "CoolKernel/FolderVersioning.h"
#include "CoolKernel/HvsTagLock.h"
#include "CoolKernel/PayloadMode.h"
#include "CoolKernel/StorageType.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

coldpie::EnumType::Enum stEnums[] = {
    {"Bool", cool::StorageType::Bool},
    {"UChar", cool::StorageType::UChar},
    {"Int16", cool::StorageType::Int16},
    {"UInt16", cool::StorageType::UInt16},
    {"Int32", cool::StorageType::Int32},
    {"UInt32", cool::StorageType::UInt32},
    {"UInt63", cool::StorageType::UInt63},
    {"Int64", cool::StorageType::Int64},
    {"Float", cool::StorageType::Float},
    {"Double", cool::StorageType::Double},
    {"String255", cool::StorageType::String255},
    {"String4k", cool::StorageType::String4k},
    {"String64k", cool::StorageType::String64k},
    {"String16M", cool::StorageType::String16M},
    {"String128M", cool::StorageType::String128M},
    {"Blob64k", cool::StorageType::Blob64k},
    {"Blob16M", cool::StorageType::Blob16M},
    {"Blob128M", cool::StorageType::Blob128M},
    {nullptr, 0}
};
coldpie::EnumType storageTypeEnum("StorageType", stEnums, "coldpie.cool");

coldpie::EnumType::Enum fvEnums[] = {
    {"NONE", cool::FolderVersioning::NONE},
    {"SINGLE_VERSION", cool::FolderVersioning::SINGLE_VERSION},
    {"MULTI_VERSION", cool::FolderVersioning::MULTI_VERSION},
    {nullptr, 0}
};
coldpie::EnumType folderVersioningEnum("FolderVersioning", fvEnums, "coldpie.cool");

coldpie::EnumType::Enum pmEnums[] = {
    {"INLINEPAYLOAD", cool::PayloadMode::INLINEPAYLOAD},
    {"SEPARATEPAYLOAD", cool::PayloadMode::SEPARATEPAYLOAD},
    {"VECTORPAYLOAD", cool::PayloadMode::VECTORPAYLOAD},
    {nullptr, 0}
};
coldpie::EnumType payloadModeEnum("PayloadMode", pmEnums, "coldpie.cool");

coldpie::EnumType::Enum tagLockEnums[] = {
    {"UNLOCKED", cool::HvsTagLock::UNLOCKED},
    {"LOCKED", cool::HvsTagLock::LOCKED},
    {"PARTIALLYLOCKED", cool::HvsTagLock::PARTIALLYLOCKED},
    {nullptr, 0}
};
coldpie::EnumType hsvTagLockEnum("HvsTagLock", tagLockEnums, "coldpie.cool");

coldpie::EnumType::Enum channelSelectionOrderEnums[] = {
    {"channelBeforeSince", cool::ChannelSelection::Order::channelBeforeSince},
    {"sinceBeforeChannel", cool::ChannelSelection::Order::sinceBeforeChannel},
    {"channelBeforeSinceDesc", cool::ChannelSelection::Order::channelBeforeSinceDesc},
    {"sinceDescBeforeChannel", cool::ChannelSelection::Order::sinceDescBeforeChannel},
    {nullptr, 0}
};
coldpie::EnumType channelSelectionOrderEnum(
    "ChannelSelectionOrder", channelSelectionOrderEnums, "coldpie.cool"
);

}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

//----------------
// Constructors --
//----------------
coldpie::EnumType&
coldpie::enums::storageTypeEnum()
{
    return ::storageTypeEnum;
}

coldpie::EnumType&
coldpie::enums::folderVersioningEnum()
{
    return ::folderVersioningEnum;
}

coldpie::EnumType&
coldpie::enums::payloadModeEnum()
{
    return ::payloadModeEnum;
}

coldpie::EnumType&
coldpie::enums::hsvTagLockEnum()
{
    return ::hsvTagLockEnum;
}

coldpie::EnumType&
coldpie::enums::channelSelectionOrderEnum()
{
    return ::channelSelectionOrderEnum;
}
