//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "CppObjectHolder.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

char typedoc[] = "Helper class to store instances of C++ types";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::CppObjectHolder::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;

  BaseType::initType("_CppObjectHolder", module, modname);
}
