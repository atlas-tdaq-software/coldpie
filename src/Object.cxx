//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "Object.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "Record.h"
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

//    virtual const ChannelId& channelId() const = 0;
PyObject* _channelId(PyObject* self, PyObject*)
try {
    auto obj = coldpie::Object::cppObject(self);
    return coldpie::cpp2py::convert(obj->channelId());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const ValidityKey& since() const = 0;
PyObject* _since(PyObject* self, PyObject*)
try {
    auto obj = coldpie::Object::cppObject(self);
    return coldpie::cpp2py::convert(obj->since());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const ValidityKey& until() const = 0;
PyObject* _until(PyObject* self, PyObject*)
try {
    auto obj = coldpie::Object::cppObject(self);
    return coldpie::cpp2py::convert(obj->until());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const IRecord& payload() const = 0;
PyObject* _payload(PyObject* self, PyObject*)
try {
    auto obj = coldpie::Object::cppObject(self);
    auto& rec = obj->payload();
    return coldpie::Record_const::PyObject_FromCpp(std::cref(rec), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//    virtual const ITime& insertionTime() const = 0;
PyObject* _insertionTime(PyObject* self, PyObject*)
try {
    auto obj = coldpie::Object::cppObject(self);
    return coldpie::cpp2py::convert(obj->insertionTime());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}


PyMethodDef methods[] = {
    {"channelId", _channelId, METH_NOARGS,
        "self.channelId() -> int\n\nReturn channel identifier."},
    {"since", _since, METH_NOARGS,
        "self.since() -> int\n\nReturn start of validity interval."},
    {"until", _until, METH_NOARGS,
        "self.until() -> int\n\nReturn end of validity interval."},
    {"payload", _payload, METH_NOARGS,
        "self.payload() -> Record_const\n\nReturn data payload as Record_const instance."},
    {"insertionTime", _insertionTime, METH_NOARGS,
        "self.insertionTime() -> Time\n\n"
        "Return time when interval was inserted into the database."},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Python wrapper for COOL IObject C++ class. It represents Interval-of-validity\n\
in COOL database and has associated since/until key values and corresponding\n\
channel identifier. Corresponding data payload object is accessed via\n\
`payload()` method.\n\
\n\
Instances of this class are created in C++ API, there is no user-accessible\n\
constructor at Python level. Use one of the methods of Folder class to produce\n\
instances of Object class.\n\
";
}

void
coldpie::Object::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;

  BaseType::initType("Object", module, modname);
}

// Dump object info to a stream
void
coldpie::Object::print(std::ostream& out) const
{
    out << "<Object@" << this << ">";
}
