#ifndef COLDPIE_OBJECTITERATOR_H
#define COLDPIE_OBJECTITERATOR_H

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/pointers.h"

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for IObjectIterator(Ptr) class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct ObjectIterator : public PyDataType<ObjectIterator, cool::IObjectIteratorPtr> {

    typedef PyDataType<ObjectIterator, cool::IObjectIteratorPtr> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;
};

} // namespace coldpie

#endif // COLDPIE_OBJECTITERATOR_H
