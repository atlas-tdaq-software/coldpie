
//-----------------------
// This Class's Header --
//-----------------------
#include "exceptions.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

//----------------
// Constructors --
//----------------
void
coldpie::translateException(const std::exception& exc)
{
    std::string msg = "coldpie error: ";
    msg += exc.what();
    PyErr_SetString(PyExc_RuntimeError, msg.c_str());
}
