#ifndef COLDPIE_RECORD_H
#define COLDPIE_RECORD_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <functional>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
#include "CoolKernel/Record.h"

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for (I)Record class (const and non-const)
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

struct Record : public PyDataType<Record, std::reference_wrapper<cool::Record>> {

    typedef PyDataType<Record, std::reference_wrapper<cool::Record>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;
};

struct Record_const : public PyDataType<Record, std::reference_wrapper<const cool::IRecord>> {

    typedef PyDataType<Record, std::reference_wrapper<const cool::IRecord>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;
};

} // namespace coldpie

#endif // COLDPIE_RECORD_H
