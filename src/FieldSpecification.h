#ifndef COLDPIE_FIELDSPECIFICATION_H
#define COLDPIE_FIELDSPECIFICATION_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//----------------------
// Base Class Headers --
//----------------------
#include "coldpie/PyDataType.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <functional>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolKernel/IFieldSpecification.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace coldpie {

/// @addtogroup coldpie

/**
 *  @ingroup coldpie
 *
 *  Python wrapper for COOL (I)FieldSpecification class
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

struct FieldSpecification : public PyDataType<FieldSpecification, std::reference_wrapper<const cool::IFieldSpecification>> {

    typedef PyDataType<FieldSpecification, std::reference_wrapper<const cool::IFieldSpecification>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const char* modname=nullptr);

    // Dump object info to a stream
    void print(std::ostream& out) const;

};

} // namespace coldpie

#endif // COLDPIE_FIELDSPECIFICATION_H
