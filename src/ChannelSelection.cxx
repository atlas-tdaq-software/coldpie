
//-----------------------
// This Class's Header --
//-----------------------
#include "ChannelSelection.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "enums.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// standard Python stuff
PyObject* _new(PyTypeObject *subtype, PyObject *args, PyObject *kwds);
PyObject* _all(PyObject* self, PyObject* args, PyObject* kwds);

// type-specific methods
// PyObject* _exists(PyObject* self, PyObject* args);


// Check that integer value for order is valid, return false and set
// Python exception if value is not valid.
bool check_order_value(int value)
{
    if (not coldpie::enums::channelSelectionOrderEnum().checkValue(value)) {
        PyErr_Format(PyExc_ValueError, "Invalid value for `order` parameter: %i", value);
        return false;
    }
    return true;
}


PyMethodDef methods[] = {
    {"all", (PyCFunction)_all, METH_VARARGS | METH_KEYWORDS | METH_STATIC,
        "ChannelSelection.all(*, order: int = ...) -> ChannelSelection"
        "\n\nReturn selector for all channels."
        "\n\nDefault value for order argument is ChannelSelection.Order.channelBeforeSince"},
    {0, 0, 0, 0}
};

char typedoc[] = "\
Helper class to specify a selection of channels and their ordering for\n\
multi-channel bulk retrieval of IOVs.\n\
\n\
Supported initialization options:\n\
\n\
   ChannelSelection(*, order: int = ...) -- selector for all existing channels\n\
   ChannelSelection(channelId: int) -- select single integer channel ID\n\
   ChannelSelection(channelName: str, *, order: int = ...) -- select one channel with given name\n\
   ChannelSelection(id1: int, id2: int, *, order: int = ...) -- channels in range id1 to id2 (inclusive)\n\
\n\
The `order` argument is keyword-only, its default value is `ChannelSelection.Order.channelBeforeSince`.\n\
\n\
Static method `ChannelSelection.all()` also returns selector which selects all\n\
existing channels.\n\
\n\
Note: instances of this class are only useful for passing to the methods of\n\
other classes, so it does not provide wrappers for regular methods.\n\
";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::ChannelSelection::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_new = ::_new;

  BaseType::initType("ChannelSelection", module, modname);

  PyObject* order_type = coldpie::enums::channelSelectionOrderEnum().type();
  PyDict_SetItemString(type->tp_dict, "Order", order_type);
}

// Dump object info to a stream
void
coldpie::ChannelSelection::print(std::ostream& out) const
{
  out << "<ChannelSelection@" << this << ">";
}

namespace {

// standard Python stuff
PyObject* _new(PyTypeObject *subtype, PyObject *args, PyObject *kwds)
{
    // parse arguments
    PyObject* arg1 = nullptr;
    PyObject* arg2 = nullptr;
    int order_int = int(cool::ChannelSelection::Order::channelBeforeSince);
    static char* keywords[] = {(char*)"", (char*)"", (char*)"order", nullptr};
    if (not PyArg_ParseTupleAndKeywords(args, kwds, "|OO$i:ChannelSelection", keywords, &arg1, &arg2, &order_int)) {
        return 0;
    }
    if (not check_order_value(order_int)) {
        return nullptr;
    }
    auto order = cool::ChannelSelection::Order(order_int);

    // with one argument we can accept int or string, with two arguments we can accept two ints
    cool::ChannelSelection chsel(order);
    try {
        if (arg1 != nullptr and arg2 != nullptr) {
            if (PyLong_Check(arg1) && PyLong_Check(arg2)) {
                // Two channel numbers.
                auto val1 = PyLong_AsLong(arg1);
                if (val1 == -1 and PyErr_Occurred() != nullptr) return nullptr;
                auto val2 = PyLong_AsLong(arg2);
                if (val2 == -1 and PyErr_Occurred() != nullptr) return nullptr;
                chsel = cool::ChannelSelection(cool::ChannelId(val1), cool::ChannelId(val2), order);
            } else {
                PyErr_SetString(PyExc_TypeError, "two integer arguments are expected");
                return nullptr;
            }
        } else if (arg1 != nullptr) {
            if (PyLong_Check(arg1)) {
                // Single channel number, order must not be specified, but we
                // ignore it here.
                auto val1 = PyLong_AsLong(arg1);
                if (val1 == -1 and PyErr_Occurred() != nullptr) return nullptr;
                chsel = cool::ChannelSelection(cool::ChannelId(val1));
            } else  if (PyUnicode_Check(arg1)) {
                chsel = cool::ChannelSelection(PyUnicode_AsUTF8(arg1), order);
            } else {
                PyErr_SetString(PyExc_TypeError, "integer or string argument is expected");
                return nullptr;
            }
        }
    } catch (std::exception const& exc) {
        PyErr_SetString(PyExc_ValueError, exc.what());
        return nullptr;
    }

    // copy fields into C++ object
    try {
        auto self = coldpie::ChannelSelection::construct(subtype, chsel);
        return self;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return nullptr;
    }
}

PyObject* _all(PyObject* self, PyObject* args, PyObject* kwds)
{
    int order_int = int(cool::ChannelSelection::Order::channelBeforeSince);
    static char* keywords[] = {(char*)"order", nullptr};
    if (not PyArg_ParseTupleAndKeywords(args, kwds, "|$i:ChannelSelection", keywords, &order_int)) {
        return 0;
    }
    if (not check_order_value(order_int)) {
        return nullptr;
    }
    auto chsel = cool::ChannelSelection::all(cool::ChannelSelection::Order(order_int));
    return coldpie::ChannelSelection::PyObject_FromCpp(std::move(chsel));
}

}
