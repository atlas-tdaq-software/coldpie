//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Description:
//  Class EnumType...
//
// Author List:
//      Andrei Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coldpie/EnumType.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <cstring>
#include <sstream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coldpie/make_pyshared.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

char docString[] = "\
Python class which emulates C++ enumeration. It defines class attributes\n\
whose values are integer numbers, so that you can use EnumType.EnumName\n\
notation to access the values.\n\
";

}

//      ----------------------------------------
//      -- Public Function Member Definitions --
//      ----------------------------------------

//----------------
// Constructors --
//----------------
coldpie::EnumType::EnumType(const char* typeName, Enum* enums)
  : EnumType(typeName, enums, nullptr)
{
}

coldpie::EnumType::EnumType(const char* typeName, Enum* enums, const char* modName)
  : m_typeName(0)
  , m_type()
  , m_docString(0)
{
  PyObject* pyTypeName = PyUnicode_FromString(typeName);
  PyObject* bases = PyTuple_Pack(1, (PyObject*)&PyBaseObject_Type);
  PyObject* dict = PyDict_New();

  // define class attributes as enum values
  for (Enum* eiter = enums; eiter->name ; ++ eiter) {

    // convert value to Python integer
    PyObject* value = PyLong_FromLong(eiter->value);

    // store it in the class attribute
    PyDict_SetItemString(dict, eiter->name, value);

    Py_CLEAR(value);
  }

  // generate doc string
  std::ostringstream doc;
  doc << ::docString << "\nThis class defines following constants:";
  for (Enum* eiter = enums; eiter->name ; ++ eiter) {
    doc << "\n    - " << eiter->name << " = " << eiter->value;
  }
  PyObject* docstr = PyUnicode_FromString(doc.str().c_str());
  PyDict_SetItemString(dict, "__doc__", docstr);
  PyObject* modname = nullptr;
  if (modName != nullptr) {
    PyObject* modname = PyUnicode_FromString(modName);
    PyDict_SetItemString(dict, "__module__", modname);
  }

  PyObject* args = PyTuple_Pack(3, pyTypeName, bases, dict);
  m_type = (PyTypeObject*)PyObject_CallObject((PyObject*)&PyType_Type, args);
  Py_CLEAR(args);
  Py_CLEAR(modname);
  Py_CLEAR(docstr);
  Py_CLEAR(dict);
  Py_CLEAR(bases);
  Py_CLEAR(pyTypeName);
}

//--------------
// Destructor --
//--------------
coldpie::EnumType::~EnumType ()
{
}

bool coldpie::EnumType::checkValue(int value) const
{
    PyObject *key = nullptr, *val = nullptr;
    Py_ssize_t pos = 0;
    while (PyDict_Next(m_type->tp_dict, &pos, &key, &val)) {
        if (PyUnicode_Check(key) and PyLong_Check(val)) {
            // Skip keys with underscores.
            if (PyUnicode_READ_CHAR(key, 0) != int('_')) {
                if (PyLong_AsLong(val) == value) {
                    return true;
                }
            }
        }
    }
    return false;
}
