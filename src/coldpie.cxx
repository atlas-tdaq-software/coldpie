/*
 *
 */

#include "Python.h"

#include "CoolKernel/ValidityKey.h"
#include "coldpie/EnumType.h"

#include "ChannelSelection.h"
#include "CppObjectHolder.h"
#include "Database.h"
#include "DatabaseSvc.h"
#include "DatabaseSvcFactory.h"
#include "HvsNode.h"
#include "Field.h"
#include "FieldSpecification.h"
#include "Folder.h"
#include "FolderSet.h"
#include "FolderSpecification.h"
#include "Object.h"
#include "ObjectIterator.h"
#include "Record.h"
#include "RecordSpecification.h"
#include "Time.h"
#include "enums.h"


namespace {

PyMethodDef methods[] = {
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "lib_coldpie",       /* m_name */
    "Python wrapper for COOL",  /* m_doc */
    -1,                  /* m_size */
    methods,             /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
};

PyObject* make_module() {
    return PyModule_Create(&moduledef);
}

PyObject* moduleinit()
{
    PyObject* this_module = ::make_module();

    // some constants
    PyModule_AddIntConstant(this_module, "ValidityKeyMin", cool::ValidityKeyMin);
    PyModule_AddIntConstant(this_module, "ValidityKeyMax", cool::ValidityKeyMax);

    PyModule_AddObject(this_module, "FolderVersioning", coldpie::enums::folderVersioningEnum().type());
    PyModule_AddObject(this_module, "HvsTagLock", coldpie::enums::hsvTagLockEnum().type());
    PyModule_AddObject(this_module, "PayloadMode", coldpie::enums::payloadModeEnum().type());
    PyModule_AddObject(this_module, "StorageType", coldpie::enums::storageTypeEnum().type());

    // define all types, order is important for some types.
    const char* modname = "coldpie.cool";
    coldpie::ChannelSelection::initType(this_module, modname);
    coldpie::CppObjectHolder::initType(this_module, modname);
    coldpie::Database::initType(this_module, modname);
    coldpie::DatabaseSvc::initType(this_module, modname);
    coldpie::DatabaseSvcFactory::initType(this_module, modname);
    coldpie::HvsNode::initType(this_module, modname);
    coldpie::Field::initType(this_module, modname);
    coldpie::Field_const::initType(this_module, modname);
    coldpie::FieldSpecification::initType(this_module, modname);
    coldpie::Folder::initType(this_module, modname);
    coldpie::FolderSet::initType(this_module, modname);
    coldpie::FolderSpecification::initType(this_module, modname);
    coldpie::Object::initType(this_module, modname);
    coldpie::ObjectIterator::initType(this_module, modname);
    coldpie::Record::initType(this_module, modname);
    coldpie::Record_const::initType(this_module, modname);
    coldpie::RecordSpecification::initType(this_module, modname);
    coldpie::Time::initType(this_module, modname);

    return this_module;
}

} // namespace

// Module entry point
extern "C"
PyMODINIT_FUNC
PyInit_lib_coldpie() {
    return ::moduleinit();
}
