//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "Record.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coldpie/make_pyshared.h"
#include "CppObjectHolder.h"
#include "Field.h"
#include "RecordSpecification.h"
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {


template <typename RecordType>
struct RecordIterData {
    RecordIterData(std::reference_wrapper<RecordType> const& rec) : record(rec) {}
    std::reference_wrapper<RecordType> record;
    unsigned pos = 0;
};

// Special iterator class for Record to iterate over contained fields
template <typename RecordType, typename FieldWrapperType>
struct RecordIter : coldpie::PyDataType<RecordIter<RecordType, FieldWrapperType>, RecordIterData<RecordType>> {

    typedef coldpie::PyDataType<RecordIter<RecordType, FieldWrapperType>, RecordIterData<RecordType>> BaseType;

    /// Initialize Python type and register it in a module
    static void initType(PyObject* module, const std::string& recordClassName, const char* modname) {
        PyTypeObject* type = BaseType::typeObject();
        type->tp_doc = "Iterator type for fields in a record instance";
        type->tp_iter = _iter;
        type->tp_iternext = _iternext;
        BaseType::initType(("_"+recordClassName+"_iter").c_str(), module, modname);
    }

    static PyObject* _iter(PyObject* self) {
        Py_INCREF(self);
        return self;
    }

    static PyObject* _iternext(PyObject* self) {
        auto& iterData = BaseType::cppObject(self);
        if (iterData.pos < iterData.record.get().size()) {
            auto& field = iterData.record.get()[iterData.pos];
            ++ iterData.pos;
            return FieldWrapperType::PyObject_FromCpp(std::ref(field), self);
        }
        // signal end of iteration
        PyErr_SetString(PyExc_StopIteration, "end of iteration");
        return nullptr;
    }

};


PyObject*
_new(PyTypeObject *subtype, PyObject *args, PyObject */*kwds*/)
try {
    // parse arguments
    PyObject* arg = nullptr;
    if (not PyArg_ParseTuple(args, "O:Record", &arg)) {
        return 0;
    }

    std::shared_ptr<cool::Record> cppRec;
    if (coldpie::RecordSpecification::Object_TypeCheck(arg)) {
        auto& spec = coldpie::RecordSpecification::cppObject(arg);
        cppRec = std::make_shared<cool::Record>(spec);
    } else if (coldpie::Record::Object_TypeCheck(arg)) {
        auto& rec = coldpie::Record::cppObject(arg);
        cppRec = std::make_shared<cool::Record>(rec);
    } else if (coldpie::Record_const::Object_TypeCheck(arg)) {
        auto& rec = coldpie::Record_const::cppObject(arg);
        cppRec = std::make_shared<cool::Record>(rec);
    } else {
        PyErr_Format(PyExc_TypeError,
                     "Record() argument must be RecordSpecification, Record, or Record_const, not %s",
                     arg->ob_type->tp_name);
        return nullptr;
    }

    PyObject* store = coldpie::CppObjectHolder::PyObject_FromCpp(cppRec);
    auto self = coldpie::Record::construct(subtype, *cppRec);
    self->owner = store;    // this steals reference
    return self;

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return 0;
}

template <typename Record>
PyObject* _specification(PyObject* self, PyObject*)
try {
    auto& rec = Record::cppObject(self).get();
    auto& spec = rec.specification();
    return coldpie::RecordSpecification::PyObject_FromCpp(std::cref(spec), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

template <typename Record, typename Field>
PyObject* _field(PyObject* self, PyObject* arg)
try {
    auto& rec = Record::cppObject(self).get();
    // can accept interger or string as a key
    if (PyLong_Check(arg)) {
        auto idx = PyLong_AsSsize_t(arg);
        if (idx < 0) {
            idx += rec.size();
        }
        try {
            auto& field = rec[idx];
            return Field::PyObject_FromCpp(std::ref(field), self);
        } catch (const cool::RecordSpecificationUnknownField& exc) {
            PyErr_SetString(PyExc_IndexError, exc.what());
            return nullptr;
        }
    } else if (const char* str = PyUnicode_AsUTF8(arg)) {
        std::string keyStr(str);
        try {
            auto& field = rec[keyStr];
            return Field::PyObject_FromCpp(std::ref(field), self);
        } catch (const cool::RecordSpecificationUnknownField& exc) {
            PyErr_SetString(PyExc_KeyError, exc.what());
            return nullptr;
        }
    } else {
        // exception is set by PyUnicode_AsUTF8
        return nullptr;
    }

} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

template <typename Record>
Py_ssize_t _length(PyObject *o)
{
    auto& rec = Record::cppObject(o).get();
    return rec.size();
}

template <typename Record>
int _contains(PyObject* self, PyObject* arg)
try {
    if (const char* name = PyUnicode_AsUTF8(arg)) {
        auto& rec = Record::cppObject(self).get();
        return rec.specification().exists(name);
    } else {
        return 0;
    }
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return -1;
}

template <typename Record, typename Field, typename Key>
PyObject* _getItemT(PyObject *o, const Key& key, PyObject* exception)
{
    auto& irec = Record::cppObject(o).get();
    try {
        auto& field = irec[key];
        return Field::data(std::ref(field), o);
    } catch (const cool::RecordSpecificationUnknownField& exc) {
        PyErr_SetString(exception, exc.what());
        return 0;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return 0;
    }
}

template <typename Record, typename Field>
PyObject* _getMapItem(PyObject *o, PyObject*  key)
{
    // can accept interger or string as a key
    if (PyLong_Check(key)) {
        auto& irec = Record::cppObject(o).get();
        auto idx = PyLong_AsSsize_t(key);
        if (idx < 0) {
            idx += irec.size();
        }
        return _getItemT<Record, Field>(o, idx, PyExc_IndexError);
    } else if (const char* str = PyUnicode_AsUTF8(key)) {
        std::string keyStr(str);
        return _getItemT<Record, Field>(o, keyStr, PyExc_KeyError);
    } else {
        // exception is set by PyUnicode_AsUTF8
        return nullptr;
    }

}

template <typename Key>
int _setItemT(PyObject *o, const Key& key, PyObject *v, PyObject* exception)
{
    auto& irec = coldpie::Record::cppObject(o).get();
    try {
        cool::IField& field = irec[key];
        return coldpie::Field::setValue(field, v);
    } catch (const cool::RecordSpecificationUnknownField& exc) {
        PyErr_SetString(exception, exc.what());
        return -1;
    } catch (const std::exception& exc) {
        coldpie::translateException(exc);
        return -1;
    }
}

int _setMapItem(PyObject *o, PyObject *key, PyObject *v)
{
    if (v == nullptr) {
        // `del` is not supported
        PyErr_SetString(PyExc_NotImplementedError, "del operation is not supported on Record fields");
        return -1;
    }
    // can accept interger or string as a key
    if (PyLong_Check(key)) {
        auto& irec = coldpie::Record::cppObject(o).get();
        auto idx = PyLong_AsSsize_t(key);
        if (idx < 0) {
            idx += irec.size();
        }
        return _setItemT(o, idx, v, PyExc_IndexError);
    } else if (const char* str = PyUnicode_AsUTF8(key)) {
        std::string keyStr(str);
        return _setItemT(o, keyStr, v, PyExc_KeyError);
    } else {
        // exception is set by PyUnicode_AsUTF8
        return -1;
    }
}

PyObject* _iter(PyObject* self)
try {
    auto& rec = coldpie::Record::cppObject(self).get();
    RecordIterData<cool::Record> iterData(rec);
    return RecordIter<cool::Record, coldpie::Field>::PyObject_FromCpp(iterData, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _iter_const(PyObject* self)
try {
    auto& rec = coldpie::Record_const::cppObject(self).get();
    RecordIterData<const cool::IRecord> iterData(rec);
    return RecordIter<const cool::IRecord, coldpie::Field_const>::PyObject_FromCpp(iterData, self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

template <typename RecordWrapperType>
PyObject *_richcompare(PyObject *self, PyObject *other, int op)
{
    if (not RecordWrapperType::Object_TypeCheck(other)) {
        Py_INCREF(Py_NotImplemented);
        return Py_NotImplemented;
    }
    auto& lhs = RecordWrapperType::cppObject(self).get();
    auto& rhs = RecordWrapperType::cppObject(other).get();
    switch (op) {
    case Py_EQ:
        return coldpie::cpp2py::convert(lhs == rhs);
        break;
    case Py_NE:
        return coldpie::cpp2py::convert(lhs != rhs);
        break;
    default:
        break;
    }
    Py_INCREF(Py_NotImplemented);
    return Py_NotImplemented;
}


PyMethodDef methods[] = {
    {"specification", _specification<coldpie::Record>, METH_NOARGS,
        "self.specification() -> RecordSpecification\n\n"
        "Return record specification associated with this record."},
    {"field", _field<coldpie::Record, coldpie::Field>, METH_O,
        "self.field(name: int | str) -> Field\n\n"
        "Return field object for given index or field name. It raises IndexError if\n"
        "integer index is out of range, and KeyError if name is not found in the record\n"
        "fields."},
    {0, 0, 0, 0}
};

PyMethodDef methods_const[] = {
    {"specification", _specification<coldpie::Record_const>, METH_NOARGS,
        "self.specification() -> RecordSpecification\n\n"
        "Return record specification associated with this record."},
    {"field", _field<coldpie::Record_const, coldpie::Field_const>, METH_O,
        "self.field(name: int | str) -> Field_const\n\n"
        "Return field object for given index or field name. It raises IndexError if\n"
        "integer index is out of range, and KeyError if name is not found in the record\n"
        "fields."},
    {0, 0, 0, 0}
};

PySequenceMethods seq_methods = {
    _length<coldpie::Record>, /* lenfunc sq_length */
    0, /* binaryfunc sq_concat */
    0, /* ssizeargfunc sq_repeat */
    0, /* ssizeargfunc sq_item; */
    0, /* ssizessizeargfunc sq_slice; */
    0, /* ssizeobjargproc sq_ass_item; */
    0, /* ssizessizeobjargproc sq_ass_slice; */
    _contains<coldpie::Record>, /* objobjproc sq_contains; */
    0, /* binaryfunc sq_inplace_concat; */
    0  /* ssizeargfunc sq_inplace_repeat; */
};

PySequenceMethods seq_methods_const = {
    _length<coldpie::Record_const>, /* lenfunc sq_length */
    0, /* binaryfunc sq_concat */
    0, /* ssizeargfunc sq_repeat */
    0, /* ssizeargfunc sq_item; */
    0, /* ssizessizeargfunc sq_slice; */
    0, /* ssizeobjargproc sq_ass_item; */
    0, /* ssizessizeobjargproc sq_ass_slice; */
    _contains<coldpie::Record_const>, /* objobjproc sq_contains; */
    0, /* binaryfunc sq_inplace_concat; */
    0  /* ssizeargfunc sq_inplace_repeat; */
};

PyMappingMethods map_methods {
    _length<coldpie::Record>, /* lenfunc mp_length */
    _getMapItem<coldpie::Record, coldpie::Field>, /* binaryfunc mp_subscript */
    _setMapItem  /* objobjargproc mp_ass_subscript */
};

PyMappingMethods map_methods_const {
    _length<coldpie::Record_const>, /* lenfunc mp_length */
    _getMapItem<coldpie::Record_const, coldpie::Field_const>, /* binaryfunc mp_subscript */
    0    /* objobjargproc mp_ass_subscript */
};

char typedoc[] = R"##(
Record represents Python wrapper for C++ Record type, instances of Record
class are modifiable and can be instantiated from Python. Record class
represent collection of Field objects and a RecordSpecification instance.
One can iterate over fields using regular iterator syntax (`for field in record:`).

Constructors for Record class:

    Record(spec: RecordSpecification)

    Parameters:
        spec : RecordSpecification
            Specification for the fields in this record.

    Record(record: Record)
    Record(record: Record_const)

    Parameters:
        record : Record or Record_const
            Contents of this record is copied into a new record.

After construction the fields in the record have their default values (0 for
numeric types, False for bool, "" for string).

Indexing operator (square brackets) has special meaning in the record class,
instead of returning corresponding Field instance it returns the field value.
It works similarly when setting field values, and this is a useful shortcut
which implements most frequent access pattern for Record class. Instances of
Fields can be accessed with a `field(...)` method. These two lines of code
perform the same operation:

    value = record.field(0).data()
    value = record[0]

And similarly for setting the value of a field:

    record.field(0).setValue(newValue)
    record[0] = newValue

or setting the value of a field to a NULL:

    record.field("id").setNull()
    record["id"] = None

Indexing operator and `field()` method support both integer indices and
strings which are the names of the fields (like in the last example).

Instances of Record class can be compared for (non-)equality using `==` or
`!=` operators. Ordering relation is not defined for this class, less/greater
operators do not work on objects.
)##";

char typedoc_const[] = R"##(
Record_const represents Python wrapper for C++ Record type, instances of
Record_const class cannot not be instantiated directly but they are returned
from other methods. Record_const instances are immutable. Record_const
represent collection of Field_const objects and a RecordSpecification
instance. One can iterate over fields using regular iterator syntax
(`for field in record:`). Indexing operator supports access by both integer
index (`record[0]`) and field name (`record["field_name"]`). Instead of
returning corresponding Field object indexing operator returns the value of
the field, see `Record` class for more details. Method `field(...)` is used
to access fields belonging to this record.

Instances of Record_const class can be compared for (non-)equality using
`==` or `!=` operators. Ordering relation is not defined for this class,
less/greater operators do not work on objects.
)##";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::Record::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_iter = ::_iter;
  type->tp_new = ::_new;
  type->tp_as_sequence = &::seq_methods;
  type->tp_as_mapping = &::map_methods;
  type->tp_richcompare = ::_richcompare<coldpie::Record>;

  BaseType::initType("Record", module, modname);
  RecordIter<cool::Record, coldpie::Field>::initType(module, "Record", modname);
}

// Dump object info to a stream
void
coldpie::Record::print(std::ostream& out) const
{
  out << "Record(" << cpp_obj.get() << ")";
}

void
coldpie::Record_const::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc_const;
  type->tp_methods = ::methods_const;
  type->tp_iter = ::_iter_const;
  type->tp_as_sequence = &::seq_methods_const;
  type->tp_as_mapping = &::map_methods_const;
  type->tp_richcompare = ::_richcompare<coldpie::Record_const>;

  BaseType::initType("Record_const", module, modname);
  RecordIter<const cool::IRecord, coldpie::Field_const>::initType(module, "Record_const", modname);
}

// Dump object info to a stream
void
coldpie::Record_const::print(std::ostream& out) const
{
  out << "Record_const(" << cpp_obj.get() << ")";
}
