//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "Field.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coldpie/CoralDefs.h"
#include "FieldSpecification.h"
#include "cpp2py.h"
#include "exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// imported coral.Blob class as Python object
PyObject* _import_coral_Blob()
{
    auto coral = PyImport_ImportModule("coral");
    if (not coral) return nullptr;
    return PyObject_GetAttrString(coral, "Blob");
}

// returns _borrowed_ reference to Blob class
PyObject* _coral_Blob() {
    static PyObject* coral_Blob(_import_coral_Blob());
    return coral_Blob;
}

// returns _new_ reference for Blob wrapper instance
PyObject* _blobOpaqueWrapper(const coral::Blob& blob) {
    // before 3_2_1 is used PyCObject, after that it switched to PyCapsule
#if COLDPIE_USE_PYCAPSULE
    return PyCapsule_New((void*)&blob, "name", nullptr);
#else
    return PyCObject_FromVoidPtr((void*)&blob, nullptr);
#endif
}

template <typename Field>
PyObject* _specification(PyObject* self, PyObject*)
try {
    auto& field = Field::cppObject(self).get();
    return coldpie::FieldSpecification::PyObject_FromCpp(field.specification(), self);
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

template <typename Field>
PyObject* _name(PyObject* self, PyObject*)
try {
    auto& field = Field::cppObject(self).get();
    return coldpie::cpp2py::convert(field.name());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

template <typename Field>
PyObject* _storageType(PyObject* self, PyObject*)
try {
    auto& field = Field::cppObject(self).get();
    return coldpie::cpp2py::convert(field.storageType().id());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

template <typename Field>
PyObject* _isNull(PyObject* self, PyObject*)
try {
    auto& field = Field::cppObject(self).get();
    return coldpie::cpp2py::convert(field.isNull());
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

PyObject* _fieldData(const cool::IField& field, PyObject* owner)
{
    if (field.isNull()) {
        Py_RETURN_NONE;
    }
    const auto& st = field.storageType();
    switch (st.id()) {
    case cool::StorageType::Bool: {
        auto val = field.template data<cool::Bool>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::UChar: {
        auto val = field.template data<cool::UChar>();
        return coldpie::cpp2py::convert(unsigned(val));
    }
    case cool::StorageType::Int16: {
        auto val = field.template data<cool::Int16>();
        return coldpie::cpp2py::convert(int(val));
    }
    case cool::StorageType::UInt16: {
        auto val = field.template data<cool::UInt16>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::Int32: {
        auto val = field.template data<cool::Int32>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::UInt32: {
        auto val = field.template data<cool::UInt32>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::UInt63: {
        auto val = field.template data<cool::UInt63>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::Int64: {
        auto val = field.template data<cool::Int64>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::Float: {
        auto val = field.template data<cool::Float>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::Double: {
        auto val = field.template data<cool::Double>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::String255:
    case cool::StorageType::String4k:
    case cool::StorageType::String64k:
    case cool::StorageType::String16M:
    case cool::StorageType::String128M: {
        const auto& val = field.template data<std::string>();
        return coldpie::cpp2py::convert(val);
    }
    case cool::StorageType::Blob64k:
    case cool::StorageType::Blob16M:
    case cool::StorageType::Blob128M: {
        const auto& val = field.template data<coral::Blob>();
        auto Blob = _coral_Blob();
        if (not Blob) return nullptr;
        // make an instance of Blob class using two-argument constructor,
        // first is parent object (self), second is pointer to C++ Blob encoded into PyCObject
        auto args = PyTuple_New(2);
        Py_INCREF(owner);                  // PyTuple_SET_ITEM steals reference, have to increment it
        PyTuple_SET_ITEM(args, 0, owner);
        PyTuple_SET_ITEM(args, 1, _blobOpaqueWrapper(val));
        auto res = PyObject_Call(Blob, args, nullptr);
        Py_CLEAR(args);
        return res;
    }
    }

    return PyErr_Format(PyExc_TypeError, "Unexpected field type: %s", field.name().c_str());
}

template <typename Field>
PyObject* _data(PyObject* self, PyObject*)
{
    auto& field = Field::cppObject(self).get();
    return _fieldData(field, self);
}

PyObject* _setNull(PyObject* self, PyObject*)
try {
    auto& field = coldpie::Field::cppObject(self).get();
    field.setNull();
    Py_RETURN_NONE;
} catch (const std::exception& exc) {
    coldpie::translateException(exc);
    return nullptr;
}

//  Note: method has flag METH_O so the only argument is an object, not a tuple
PyObject* _setValue(PyObject* self, PyObject* arg)
{
    auto& field = coldpie::Field::cppObject(self).get();
    if (coldpie::Field::setValue(field, arg) < 0) {
        return nullptr;
    }
    Py_RETURN_NONE;
}

PyObject *_richcompare(PyObject *self, PyObject *other, int op)
{
    if (not coldpie::Field::Object_TypeCheck(other)) {
        Py_INCREF(Py_NotImplemented);
        return Py_NotImplemented;
    }
    auto& lhs = coldpie::Field::cppObject(self).get();
    auto& rhs = coldpie::Field::cppObject(other).get();
    switch (op) {
    case Py_EQ:
        return coldpie::cpp2py::convert(lhs == rhs);
        break;
    case Py_NE:
        return coldpie::cpp2py::convert(lhs != rhs);
        break;
    default:
        break;
    }
    Py_INCREF(Py_NotImplemented);
    return Py_NotImplemented;
}


PyMethodDef methods[] = {
    {"specification", _specification<coldpie::Field>, METH_NOARGS,
        "self.specification() -> FieldSpecification\n\nReturn field specification."},
    {"name", _name<coldpie::Field>, METH_NOARGS,
        "self.name() -> str\n\nReturn field name."},
    {"storageType", _storageType<coldpie::Field>, METH_NOARGS,
        "self.storageType() -> int\n\n"
        "Return storage type of the field.\n\n"
        "Returned value is one of the values defined in StorageType class."},
    {"isNull", _isNull<coldpie::Field>, METH_NOARGS,
        "self.isNull() -> bool\n\nReturn True if field value is NULL."},
    {"data", _data<coldpie::Field>, METH_NOARGS,
        "self.data() -> Any\n\nReturn field value.\n\n"
        "None is returned if field value is NULL, otherwise instance of correct type is\n"
        "returned (e.g. string, int, float, coral.Blob)."},
    {"setNull", _setNull, METH_NOARGS,
        "self.setNull()\n\nSet the value of this field to NULL, any previous value is lost."},
    {"setValue", _setValue, METH_O,
        "self.setValue(value: Any)\n\n"
        "Set the value of this field to a specified value. Raises TypeError exception if\n"
        "type of new value does not match field type. Passing None as a parameter is\n"
        "equivalent to calling `setNull()`."},
    {0, 0, 0, 0}
};

PyMethodDef methods_const[] = {
    {"specification", _specification<coldpie::Field_const>, METH_NOARGS,
        "self.specification() -> FieldSpecification\n\nReturn field specification."},
    {"name", _name<coldpie::Field_const>, METH_NOARGS,
        "self.name() -> str\n\nReturn field name."},
    {"storageType", _storageType<coldpie::Field_const>, METH_NOARGS,
        "self.storageType() -> int\n\n"
        "Return storage type of the field.\n\n"
        "Returned value is one of the values defined in StorageType class."},
    {"isNull", _isNull<coldpie::Field_const>, METH_NOARGS,
        "self.isNull() -> bool\n\nReturn True if field value is NULL."},
    {"data", _data<coldpie::Field_const>, METH_NOARGS,
        "self.data() -> Any\n\nReturn field value.\n\n"
        "None is returned if field value is NULL, otherwise instance of correct type is\n"
        "returned (e.g. string, int, float, coral.Blob)."},
    {0, 0, 0, 0}
};

char typedoc[] = R"##(
Field class represents modifiable field in a Record class.

Instances of this class are created in C++ API, there is no user-accessible
constructor at Python level.

Clients can access the name of the field using `name()` method. Data
associated with the field can be accessed with `data()` method. Data can be
modified using `setValue(...)` or `setNull()` methods. Instances of Field
class can be compared for (non-)equality using `==` or `!=` operators.
Ordering relation is not defined for this class, less/greater operators do
not work on objects.
)##";

char typedoc_const[] = R"##(
Field_const class represents non-modifiable field in a Record_cont class.

Instances of this class are created in C++ API, there is no user-accessible
constructor at Python level.

Clients can access the name of the field using `name()` method. Data
associated with the field can be accessed with `data()` method. Instances of
Field_const class can be compared for (non-)equality using `==` or `!=`
operators. Ordering relation is not defined for this class, less/greater
operators do not work on objects.
)##";
}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

void
coldpie::Field::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc;
  type->tp_methods = ::methods;
  type->tp_richcompare = ::_richcompare;

  BaseType::initType("Field", module, modname);
}

int coldpie::Field::setValue(cool::IField& field, PyObject* arg)
{
    if (arg == Py_None) {
        field.setNull();
        return 0;
    }

    const auto& st = field.storageType();

    switch (st.id()) {
    case cool::StorageType::Bool: {
        long val = PyLong_AsLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::Bool>(bool(val));
        break;
    }
    case cool::StorageType::UChar: {
        long val = PyLong_AsLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::UChar>(val);
        break;
    }
    case cool::StorageType::Int16: {
        long val = PyLong_AsLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::Int16>(val);
        break;
    }
    case cool::StorageType::UInt16: {
        long val = PyLong_AsLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::UInt16>(val);
        break;
    }
    case cool::StorageType::Int32: {
        long val = PyLong_AsLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::Int32>(val);
        break;
    }
    case cool::StorageType::UInt32: {
        long val = PyLong_AsLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::UInt32>(val);
        break;
    }
    case cool::StorageType::UInt63: {
        unsigned PY_LONG_LONG val;
        val = PyLong_AsUnsignedLongLong(arg);
        if (val == (unsigned PY_LONG_LONG)-1 and PyErr_Occurred()) return -1;
        try {
            field.setValue<cool::UInt63>(val);
        } catch (const std::exception& exc) {
            PyErr_SetString(PyExc_ValueError, exc.what());
            return -1;
        }
        break;
    }
    case cool::StorageType::Int64: {
        PY_LONG_LONG val;
        val = PyLong_AsLongLong(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::Int64>(val);
        break;
    }
    case cool::StorageType::Float: {
        double val = PyFloat_AsDouble(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::Float>(val);
        break;
    }
    case cool::StorageType::Double: {
        double val = PyFloat_AsDouble(arg);
        if (val == -1 and PyErr_Occurred()) return -1;
        field.setValue<cool::Double>(val);
        break;
    }
    case cool::StorageType::String255:
    case cool::StorageType::String4k:
    case cool::StorageType::String64k:
    case cool::StorageType::String16M:
    case cool::StorageType::String128M: {
        const char* buf = PyUnicode_AsUTF8(arg);
        if (buf == nullptr) return -1;
        field.setValue<std::string>(std::string(buf));
        break;
    }
    case cool::StorageType::Blob64k:
    case cool::StorageType::Blob16M:
    case cool::StorageType::Blob128M: {
        // here we accept any buffer as argument, this is not very efficient because
        // it needs a copy of that data in coral::Blob
        auto isBuffer = PyObject_CheckBuffer(arg);
        if (not isBuffer) {
            PyErr_SetString(PyExc_TypeError, "Field.setValue(): expect buffer instance as argument");
            return -1;
        }

        Py_buffer pybuffer;
        int res = PyObject_GetBuffer(arg, &pybuffer, PyBUF_SIMPLE);
        if (res == -1) return -1;
        const char *buffer = static_cast<const char*>(pybuffer.buf);
        Py_ssize_t buffer_len = pybuffer.len;

        coral::Blob blob(buffer_len);
        std::copy(buffer, buffer+buffer_len, static_cast<char*>(blob.startingAddress()));
        PyBuffer_Release(&pybuffer);

        field.setValue<coral::Blob>(blob);
        break;
    }
    default:
        PyErr_Format(PyExc_TypeError, "Unexpected field type: %s", field.name().c_str());
        return -1;
    }

    return 0;
}

PyObject*
coldpie::Field::data(const cool::IField& field, PyObject* owner)
{
    return ::_fieldData(field, owner);
}

int
coldpie::Field::setValue(PyObject* arg)
{
    auto& field = this->cpp_obj.get();
    return coldpie::Field::setValue(field, arg);
}

PyObject*
coldpie::Field::data()
{
    auto& field = this->cpp_obj.get();
    return ::_fieldData(field, static_cast<PyObject*>(this));
}

// Dump object info to a stream
void
coldpie::Field::print(std::ostream& out) const
{
  out << "Field(" << cpp_obj.get() << ")";
}

void
coldpie::Field_const::initType(PyObject* module, const char* modname)
{
  PyTypeObject* type = BaseType::typeObject();
  type->tp_doc = ::typedoc_const;
  type->tp_methods = ::methods_const;

  BaseType::initType("Field_const", module, modname);
}

PyObject*
coldpie::Field_const::data(const cool::IField& field, PyObject* owner)
{
    return ::_fieldData(field, owner);
}

PyObject*
coldpie::Field_const::data()
{
    auto& field = this->cpp_obj.get();
    return ::_fieldData(field, static_cast<PyObject*>(this));
}

// Dump object info to a stream
void
coldpie::Field_const::print(std::ostream& out) const
{
  out << "Field_const(" << cpp_obj.get() << ")";
}
