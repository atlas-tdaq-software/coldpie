import sys
if sys.platform == 'linux2':
  # python2 on Linux
  import DLFCN
  sys.setdlopenflags( DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL )
  from lib_coldpie import *
  import lib_coldpie
  __doc__ = lib_coldpie.__doc__
  del lib_coldpie
  del DLFCN
elif sys.platform == 'linux':
  # python 3, DLFCN replaced by os
  import os
  sys.setdlopenflags( os.RTLD_NOW | os.RTLD_GLOBAL )
  from lib_coldpie import *
  import lib_coldpie
  __doc__ = lib_coldpie.__doc__
  del lib_coldpie
  del os
else:
  if sys.platform == 'darwin':
    from lib_coldpie import *
    import lib_coldpie
    __doc__ = lib_coldpie.__doc__
    del lib_coldpie
  else:
    from _coldpie import *
    import _coldpie
    __doc__ = _coldpie.__doc__
    del _coldpie
del sys
