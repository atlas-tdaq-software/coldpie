Python wrapper for COOL
=======================

This is one more implementation of Python wrapper for COOL. Difference from
standard PyCOOL wrapper is that it does not use PyROOT and will be free from
the problems associated with it.

Initally only a small subset of COOL API will be implemented necessary to
implement OLC2COOL.